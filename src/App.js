import React, {Component} from 'react';
import Login from './components/Login';
import MainNavigator from './MainNavigator';
import {View, Text, TouchableOpacity} from 'react-native';
import {LoginManager} from 'react-native-fbsdk';

console.disableYellowBox = true;

class App extends Component {
  loginFacebook = () => {
    LoginManager.logInWithPermissions(['public_profile']).then(
      function(result) {
        if (result.isCancelled) {
          console.log('Login cancelled');
        } else {
          console.log(
            'Login success with permissions: ' +
              result.grantedPermissions.toString(),
          );
        }
      },
      function(error) {
        console.log('Login fail with error: ' + error);
      },
    );
  };

  render() {
    return <MainNavigator />;
  }
}

export default App;
