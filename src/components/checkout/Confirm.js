import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Image,
  Alert,
  ActivityIndicator,
} from 'react-native';
import axios from 'axios';
import {Icon} from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
class Confirm extends Component {
  state = {
    customerData: {},
    firstName: '',
    lastName: '',
    checkoutOrderData: {},
    cart: [],
    prescriptionImage: null,
    loading: false,
    buttonDisable: false,
    totalPrice: 0,
  };

  componentDidMount = async () => {
    let customerData = await AsyncStorage.getItem('customerData');
    customerData = JSON.parse(customerData);

    let cart = await AsyncStorage.getItem('cart');
    cart = JSON.parse(cart);

    let checkoutOrderData = await AsyncStorage.getItem('checkoutOrderData');
    checkoutOrderData = JSON.parse(checkoutOrderData);

    let prescriptionImage = await AsyncStorage.getItem('prescriptionImage');
    prescriptionImage = JSON.parse(prescriptionImage);

    let imageFrom = await AsyncStorage.getItem('imageFrom');

    console.log('Image From:', imageFrom);
    this.setState({customerData: customerData});
    this.setState({checkoutOrderData: checkoutOrderData});
    this.setState({cart: cart});
    this.setState({prescriptionImage: prescriptionImage});
    console.log('Rap', this.state.prescriptionImage);
    console.log('data', this.state.checkoutOrderData);
    let totalPrice =
      parseFloat(this.state.checkoutOrderData.deliveryFee) +
      parseFloat(this.state.checkoutOrderData.total);
    this.setState({totalPrice: totalPrice});
  };

  renderItem = () => {
    return this.state.cart.map((i, index) => {
      return (
        <View
          key={i._id}
          style={{
            flexDirection: 'row',
            marginTop: 10,
          }}>
          <View style={{flex: 0.4, height: 120}}>
            <Image
              source={{
                uri: `https://pharmakeia.herokuapp.com/${
                  i.image
                }?${Date.now()}`,
              }}
              style={{height: '100%', width: '100%'}}
            />
          </View>
          <View style={{flex: 0.6, padding: 10}}>
            <Text>{i.productTitle}</Text>
            <View style={{flexDirection: 'row'}}>
              <Text>Qty:</Text>
              <Text style={{marginLeft: 5}}>{i.quantity}</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text>Price:</Text>
              <Text style={{marginLeft: 5}}>₱ {i.price.toFixed(2)}</Text>
            </View>
          </View>
        </View>
      );
    });
  };

  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'whitesmoke'}}>
        <View style={{flex: 0.2, backgroundColor: 'white'}}>
          <View
            style={{
              flex: 0.4,
              justifyContent: 'center',
            }}>
            <Text style={{fontSize: 20, alignSelf: 'center'}}>Checkout</Text>
          </View>
          <View
            style={{
              flex: 0.6,
              flexDirection: 'row',
            }}>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Shipping')}>
                <Icon name="map-marker" type="font-awesome" size={30} />
                <Text>Shipping</Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 1,

                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Payment')}>
                <Icon name="money" type="font-awesome" size={30} />
                <Text>Payment</Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <TouchableOpacity>
                <Icon
                  name="check-circle"
                  type="font-awesome"
                  size={30}
                  color="#1fa5f2"
                />
                <Text
                  style={{
                    color: '#1fa5f2',
                  }}>
                  Confirm
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={{flex: 0.7}}>
          <ScrollView contentContainerStyle={{padding: 10}}>
            <Text
              style={{
                fontSize: 20,
                fontFamily: 'Roboto-Bold',
                marginBottom: 10,
              }}>
              Products
            </Text>
            {this.renderItem()}
            <Text
              style={{
                fontSize: 20,
                fontFamily: 'Roboto-Bold',
                marginBottom: 10,
              }}>
              Prescription
            </Text>
            {this.state.prescriptionImage ? (
              <View
                style={{width: '100%', height: 500, justifyContent: 'center'}}>
                <Image
                  source={{uri: this.state.prescriptionImage.uri}}
                  style={{width: '100%', height: '100%'}}
                />
              </View>
            ) : (
              <View>
                <Text>Prescription Not required</Text>
              </View>
            )}

            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 30,
                padding: 5,
              }}>
              <Text style={{flex: 0.5}}>Full Name:</Text>
              <Text style={{flex: 0.5}}>{`${
                this.state.customerData.firstName
              } ${this.state.customerData.lastName}`}</Text>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 10,
                padding: 5,
              }}>
              <Text style={{flex: 0.5}}>Address:</Text>
              <Text style={{flex: 0.5}}>
                {this.state.checkoutOrderData.address}
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 10,
                padding: 5,
              }}>
              <Text style={{flex: 0.5}}>Delivery Fee:</Text>
              <Text style={{flex: 0.5}}>
                ₱
                {this.state.checkoutOrderData.deliveryFee
                  ? this.state.checkoutOrderData.deliveryFee.toFixed(2)
                  : '0'}
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 10,
                padding: 5,
              }}>
              <Text style={{flex: 0.5}}>Payment:</Text>
              <Text style={{flex: 0.5}}>
                {this.state.checkoutOrderData.payment === 'cod'
                  ? 'Cash On Delivery'
                  : 'Credit Card'}
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 10,
                padding: 5,
              }}>
              <Text style={{flex: 0.5}}>Orders:</Text>
              <Text style={{flex: 0.5}}>
                {this.state.checkoutOrderData.orderCount}
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 10,
                padding: 5,
              }}>
              <Text style={{flex: 0.5, fontSize: 20, fontWeight: 'bold'}}>
                Total Price:
              </Text>
              <Text style={{flex: 0.5, fontSize: 20, fontWeight: 'bold'}}>
                ₱ {this.state.totalPrice}
              </Text>
            </View>
          </ScrollView>
        </View>
        <View
          style={{
            flex: 0.1,
            backgroundColor: '#1fa5f2',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <TouchableOpacity
            disabled={this.state.buttonDisable}
            onPress={async () => {
              this.setState({
                loading: true,
                buttonDisable: true,
              });

              let customerData = await AsyncStorage.getItem('customerData');
              let imageFrom = await AsyncStorage.getItem('imageFrom');
              customerData = JSON.parse(customerData);

              let prescriptionImage = await AsyncStorage.getItem(
                'prescriptionImage',
              );
              this.state.checkoutOrderData['imageFrom'] = imageFrom;
              prescriptionImage = JSON.parse(prescriptionImage);

              console.log(prescriptionImage);
              const data = new FormData();
              console.log('@@@', this.state.checkoutOrderData);
              console.log('@@@@@', this.state.prescriptionImage);
              this.state.checkoutOrderData.total =
                parseInt(this.state.checkoutOrderData.total) +
                this.state.checkoutOrderData.deliveryFee;
              console.log('Line 282: ', this.state.checkoutOrderData);
              if (!prescriptionImage) {
                console.log('PUmasok');
                axios
                  .post(
                    `https://pharmakeia.herokuapp.com/order`,
                    this.state.checkoutOrderData,
                  )
                  .then(res => {
                    console.log(res);
                    AsyncStorage.removeItem('cart');
                    AsyncStorage.removeItem('checkoutOrderData');
                    AsyncStorage.removeItem('prescriptionImage');
                    setTimeout(() => {
                      Alert.alert('Order Submitted');
                      this.setState({
                        loading: false,
                        buttonDisable: false,
                      });
                      this.props.navigation.popToTop();
                    }, 1000);
                  })
                  .catch(error => {
                    console.log(error);
                    this.setState({
                      loading: false,
                      buttonDisable: false,
                    });
                  });
              } else {
                let id;
                axios
                  .post(
                    'https://pharmakeia.herokuapp.com/order',
                    this.state.checkoutOrderData,
                  )
                  .then(res => {
                    // console.log(res);
                    id = res.data._id;
                    data.append('prescription', {
                      uri: this.state.prescriptionImage.uri,
                      type: this.state.prescriptionImage.type,
                      name: this.state.prescriptionImage.fileName,
                    });
                    return axios.post(
                      `https://pharmakeia.herokuapp.com/order/${id}/upload/image`,
                      data,
                      {
                        headers: {
                          Accept: 'application/json',
                          'Content-Type': 'multipart/form-data',
                        },
                      },
                    );
                  })
                  .then(res => {
                    console.log('311', res);
                    // return PUT
                    return axios.put(
                      `https://pharmakeia.herokuapp.com/order/${id}`,
                      {prescriptionImage: res.data},
                    );
                  })
                  .then(res => {
                    console.log('269:', res);
                    Alert.alert('Order Submitted');
                    AsyncStorage.removeItem('cart');
                    AsyncStorage.removeItem('imageFrom');
                    AsyncStorage.removeItem('checkoutOrderData');
                    AsyncStorage.removeItem('prescriptionImage');
                    this.props.navigation.popToTop();
                    setTimeout(() => {
                      this.setState({
                        loading: false,
                        buttonDisable: false,
                      });
                    }, 1000);
                  })
                  .catch(error => {
                    console.log('316', error.response);
                  });
                this.setState({
                  loading: false,
                  buttonDisable: false,
                });

                return;
              }
            }}
            style={{
              width: '100%',
              height: '100%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            {this.state.loading ? (
              <ActivityIndicator size="small" color="white" />
            ) : (
              <Text style={{color: 'white', fontSize: 20}}>Save</Text>
            )}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default Confirm;
