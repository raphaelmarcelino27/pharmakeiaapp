import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    TextInput,
    Alert,
    ActivityIndicator
} from 'react-native';
import { Icon } from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
class Shipping extends Component {
    state = {
        customerData: {},
        city: '',
        zip: '',
        address: '',
        barangay: '',
        paymentButtonHeaderDisable: true,
        confirmButtonHeaderDisable: true,
        loading: false,
        buttonDisable: false
    };
    componentDidMount = async () => {
        let customerData = await AsyncStorage.getItem('customerData');
        let checkoutCustomerData = await AsyncStorage.getItem(
            'checkoutCustomerData'
        );
        customerData = JSON.parse(customerData);
        console.log('Line 25', customerData);

        let prescriptionImage = await AsyncStorage.getItem('prescriptionImage');
        prescriptionImage = JSON.parse(prescriptionImage);
        console.log(prescriptionImage);

        this.setState({ customerData: customerData });
    };

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'whitesmoke' }}>
                <View style={{ flex: 0.2, backgroundColor: 'white' }}>
                    <View
                        style={{
                            flex: 0.4,
                            justifyContent: 'center'
                        }}
                    >
                        <Text style={{ fontSize: 20, alignSelf: 'center' }}>
                            Checkout
                        </Text>
                    </View>
                    <View
                        style={{
                            flex: 0.6,
                            flexDirection: 'row'
                        }}
                    >
                        <View
                            style={{
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}
                        >
                            <TouchableOpacity disabled={true}>
                                <Icon
                                    name="map-marker"
                                    type="font-awesome"
                                    size={30}
                                    color="#1fa5f2"
                                />
                                <Text style={{ color: '#1fa5f2' }}>
                                    Shipping
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View
                            style={{
                                flex: 1,

                                justifyContent: 'center',
                                alignItems: 'center'
                            }}
                        >
                            <TouchableOpacity
                                disabled={this.state.paymentButtonHeaderDisable}
                                onPress={() =>
                                    this.props.navigation.navigate('Payment')
                                }
                            >
                                <Icon
                                    name="money"
                                    type="font-awesome"
                                    size={30}
                                />
                                <Text>Payment</Text>
                            </TouchableOpacity>
                        </View>
                        <View
                            style={{
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}
                        >
                            <TouchableOpacity
                                disabled={this.state.confirmButtonHeaderDisable}
                                onPress={() =>
                                    this.props.navigation.navigate('Confirm')
                                }
                            >
                                <Icon
                                    name="check-circle"
                                    type="font-awesome"
                                    size={30}
                                />
                                <Text>Confirm</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={{ flex: 0.7 }}>
                    <ScrollView style={{ paddingVertical: 50 }}>
                        <TextInput
                            value={this.state.city}
                            onChangeText={text => {
                                if (text === 'Santa Rosa') {
                                    this.setState({ zip: '4026' });
                                }
                                if (text === 'Cabuyao') {
                                    this.setState({ zip: '4025' });
                                }
                                if (text === 'Binan') {
                                    this.setState({ zip: '4024' });
                                }
                                if (text === 'San Pedro') {
                                    this.setState({ zip: '4023' });
                                }
                                this.setState({ city: text });
                            }}
                            placeholder="City"
                            multiline
                            style={{
                                borderBottomWidth: 1,
                                padding: 20,
                                width: '100%'
                            }}
                        />
                        <TextInput
                            value={this.state.barangay}
                            onChangeText={text => {
                                this.setState({ barangay: text });
                            }}
                            placeholder="Barangay"
                            style={{ borderBottomWidth: 1, padding: 20 }}
                        />
                        <TextInput
                            value={this.state.zip}
                            onChangeText={text => {
                                if (text === '4025') {
                                    this.setState({ city: 'Cabuyao' });
                                }
                                if (text === '4024') {
                                    this.setState({ city: 'Binan' });
                                }
                                if (text === '4023') {
                                    this.setState({ city: 'Santa Rosa' });
                                }
                                if (text === '4026') {
                                    this.setState({ city: 'San Pedro' });
                                }

                                this.setState({ zip: text });
                            }}
                            placeholder="Zip"
                            maxLength={4}
                            keyboardType="number-pad"
                            style={{ borderBottomWidth: 1, padding: 20 }}
                        />
                        <TextInput
                            value={this.state.address}
                            onChangeText={text => {
                                this.setState({ address: text });
                            }}
                            placeholder="Street Address"
                            multiline
                            style={{
                                borderBottomWidth: 1,
                                padding: 20,
                                height: 100
                            }}
                        />
                    </ScrollView>
                </View>
                <View
                    style={{
                        flex: 0.1,
                        backgroundColor: '#1fa5f2',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                >
                    <TouchableOpacity
                        disabled={this.state.buttonDisable}
                        onPress={async () => {
                            this.setState({
                                loading: true,
                                buttonDisable: true
                            });
                            if (
                                this.state.barangay.length != 0 &&
                                this.state.city.length != 0 &&
                                this.state.zip.length != 0 &&
                                this.state.address.length != 0
                            ) {
                                if (
                                    this.state.zip === '4025' ||
                                    this.state.zip === '4024' ||
                                    this.state.zip === '4026' ||
                                    this.state.zip === '4023'
                                ) {
                                    let checkoutOrderData = await AsyncStorage.getItem(
                                        'checkoutOrderData'
                                    );

                                    checkoutOrderData = JSON.parse(
                                        checkoutOrderData
                                    );
                                    if (this.state.zip == 4025) {
                                        checkoutOrderData['deliveryFee'] = 53;
                                    }
                                    if (this.state.zip == 4024) {
                                        checkoutOrderData['deliveryFee'] = 49;
                                    }
                                    if (this.state.zip == 4026) {
                                        checkoutOrderData['deliveryFee'] = 37;
                                    }
                                    if (this.state.zip == 4023) {
                                        checkoutOrderData['deliveryFee'] = 55;
                                    }

                                    checkoutOrderData[
                                        'address'
                                    ] = `${this.state.zip} ${this.state.address} Brgy. ${this.state.barangay} City of ${this.state.city}`;

                                    console.log(
                                        'Shipping LIne 150: ',
                                        checkoutOrderData
                                    );

                                    AsyncStorage.setItem(
                                        'checkoutOrderData',
                                        JSON.stringify(checkoutOrderData)
                                    );
                                    setTimeout(() => {
                                        this.setState({
                                            paymentButtonHeaderDisable: false,
                                            loading: false,
                                            buttonDisable: false
                                        });
                                        this.props.navigation.navigate(
                                            'Payment'
                                        );
                                    }, 2000);
                                } else {
                                    Alert.alert('Invalid Address');
                                    this.setState({
                                        loading: false,
                                        buttonDisable: false
                                    });
                                }
                            } else {
                                Alert.alert('Please fill all the fields');
                                this.setState({
                                    loading: false,
                                    buttonDisable: false
                                });
                            }
                        }}
                        style={{
                            width: '100%',
                            height: '100%',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                    >
                        {this.state.loading ? (
                            <ActivityIndicator size="small" color="white" />
                        ) : (
                            <Text style={{ color: 'white', fontSize: 20 }}>
                                Save
                            </Text>
                        )}
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

export default Shipping;
