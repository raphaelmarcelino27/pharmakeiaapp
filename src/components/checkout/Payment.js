import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    TextInput,
    Alert,
    ActivityIndicator
} from 'react-native';
import { Icon } from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
class Payment extends Component {
    state = {
        creditCard: false,
        cod: false,
        confirmButtonHeaderDisable: true,
        loading: false,
        buttonDisable: false
    };

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'whitesmoke' }}>
                <View style={{ flex: 0.2, backgroundColor: 'white' }}>
                    <View
                        style={{
                            flex: 0.4,
                            justifyContent: 'center'
                        }}
                    >
                        <Text style={{ fontSize: 20, alignSelf: 'center' }}>
                            Checkout
                        </Text>
                    </View>
                    <View
                        style={{
                            flex: 0.6,
                            flexDirection: 'row'
                        }}
                    >
                        <View
                            style={{
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}
                        >
                            <TouchableOpacity
                                onPress={() =>
                                    this.props.navigation.navigate('Shipping')
                                }
                            >
                                <Icon
                                    name="map-marker"
                                    type="font-awesome"
                                    size={30}
                                />
                                <Text>Shipping</Text>
                            </TouchableOpacity>
                        </View>
                        <View
                            style={{
                                flex: 1,

                                justifyContent: 'center',
                                alignItems: 'center'
                            }}
                        >
                            <TouchableOpacity>
                                <Icon
                                    name="money"
                                    type="font-awesome"
                                    size={30}
                                    color="#1fa5f2"
                                />
                                <Text style={{ color: '#1fa5f2' }}>
                                    Payment
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View
                            style={{
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}
                        >
                            <TouchableOpacity
                                disabled={this.state.confirmButtonHeaderDisable}
                                onPress={() =>
                                    this.props.navigation.navigate('Confirm')
                                }
                            >
                                <Icon
                                    name="check-circle"
                                    type="font-awesome"
                                    size={30}
                                />
                                <Text>Confirm</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View
                    style={{
                        flex: 0.7,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                >
                    <TouchableOpacity
                        onPress={() =>
                            this.setState({ creditCard: true, cod: false })
                        }
                        style={{
                            width: '80%',
                            paddingVertical: 20,
                            backgroundColor: this.state.creditCard
                                ? '#1fa5f2'
                                : 'white',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                    >
                        <Text
                            style={{
                                color: this.state.creditCard ? 'white' : 'black'
                            }}
                        >
                            Credit Card
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() =>
                            this.setState({ cod: true, creditCard: false })
                        }
                        style={{
                            width: '80%',
                            paddingVertical: 20,
                            backgroundColor: this.state.cod
                                ? '#1fa5f2'
                                : 'white',
                            alignItems: 'center',
                            justifyContent: 'center',
                            marginTop: 10
                        }}
                    >
                        <Text
                            style={{
                                color: this.state.cod ? 'white' : 'black'
                            }}
                        >
                            Cash On Delivery
                        </Text>
                    </TouchableOpacity>
                </View>
                <View
                    style={{
                        flex: 0.1,
                        backgroundColor: '#1fa5f2',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                >
                    <TouchableOpacity
                        disabled={this.state.buttonDisable}
                        onPress={async () => {
                            this.setState({
                                loading: true,
                                buttonDisable: true
                            });
                            if (this.state.cod || this.state.creditCard) {
                                let checkoutOrderData = await AsyncStorage.getItem(
                                    'checkoutOrderData'
                                );
                                checkoutOrderData = JSON.parse(
                                    checkoutOrderData
                                );

                                checkoutOrderData['payment'] = this.state.cod
                                    ? 'cod'
                                    : 'creditCard';
                                console.log(checkoutOrderData);

                                AsyncStorage.setItem(
                                    'checkoutOrderData',
                                    JSON.stringify(checkoutOrderData)
                                );

                                setTimeout(() => {
                                    this.setState({
                                        loading: false,
                                        buttonDisable: false
                                    });
                                    checkoutOrderData.payment === 'cod'
                                        ? this.props.navigation.navigate(
                                              'Confirm'
                                          )
                                        : this.props.navigation.navigate(
                                              'CreditCard'
                                          );
                                }, 2000);
                            } else {
                                Alert.alert('Please Choose a mode of payment');
                            }
                        }}
                        style={{
                            width: '100%',
                            height: '100%',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                    >
                        {this.state.loading ? (
                            <ActivityIndicator size="small" color="white" />
                        ) : (
                            <Text style={{ color: 'white', fontSize: 20 }}>
                                Save
                            </Text>
                        )}
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

export default Payment;
