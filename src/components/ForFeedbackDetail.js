import React, { Component } from 'react';
import {
    View,
    Text,
    FlatList,
    Image,
    TouchableOpacity,
    TextInput,
    Alert
} from 'react-native';
import axios from 'axios';
import { AirbnbRating, Rating } from 'react-native-ratings';
import Modal from 'react-native-modal';
import AsyncStorage from '@react-native-community/async-storage';

class ForFeedbackDetail extends Component {
    state = {
        data: [],
        rate: {},
        comment: {},
        toSubmitData: [],
        customerData: {}
    };

    async componentDidMount() {
        let customerData = await AsyncStorage.getItem('customerData');
        customerData = JSON.parse(customerData);

        this.setState({ customerData: customerData });
        this.setState({
            data: this.props.navigation.state.params.item
        });

        console.log(this.props.navigation.state.params.item);
        let toSubmitData = this.state.toSubmitData;
        let data = this.props.navigation.state.params.item.orders;

        console.log(toSubmitData);

        data.forEach((x, index) => {
            console.log('x', x);
            console.log('index', index);
            if (index === 0) {
                let obj = {
                    rating: 5,
                    customer: customerData._id,
                    product: x.product._id
                };
                toSubmitData.push(obj);
            } else {
                let obj = {
                    rating: 5,
                    customer: customerData._id,
                    product: x.product._id
                };
                toSubmitData[index] = obj;
            }
        });
        this.setState({ toSubmitData: toSubmitData });
    }

    renderOrders = ({ item, index }) => {
        const {
            quantity,
            _id,
            price,
            total,
            product: { _id: productId, image, productNo, productTitle }
        } = item;
        return (
            <View
                style={{
                    width: '100%',
                    flexDirection: 'row',
                    marginBottom: 10,
                    backgroundColor: 'white',
                    shadowOffset: { width: 0, height: 0 },
                    shadowColor: 'black',
                    shadowOpacity: 0.2,
                    elevation: 3
                }}
            >
                <View
                    style={{ alignItems: 'center', justifyContent: 'center' }}
                >
                    <Image
                        style={{
                            height: 100,
                            width: 100,
                            borderColor: 'black'
                        }}
                        source={{
                            uri: `https://pharmakeia.herokuapp.com/${image}`
                        }}
                    />
                </View>
                <View
                    style={{
                        flex: 1,
                        alignItems: 'flex-start',
                        justifyContent: 'center',
                        paddingHorizontal: 10
                    }}
                >
                    <Text style={{ fontWeight: 'bold' }}>{productTitle}</Text>
                    {/* <Text style={{ color: 'gray' }}>{productNo}</Text>

                    <Text style={{}}>{`₱ ${price.toFixed(2)}`}</Text> */}

                    {/* <View
                        style={{
                            alignItems: 'center',
                            justifyContent: 'flex-end',
                            flexDirection: 'row',
                            width: '100%'
                        }}
                    > */}
                    <AirbnbRating
                        style={{ width: '50%' }}
                        defaultRating={5}
                        type="star"
                        ratingCount={5}
                        size={20}
                        showRating={false}
                        onFinishRating={rate => {
                            let toSubmitData = this.state.toSubmitData;
                            let rating = rate;
                            toSubmitData[index] = {
                                ...toSubmitData[index],
                                rating: rating
                            };
                            this.setState({ toSubmitData: toSubmitData });
                        }}
                    />
                    {/* </View> */}
                    <TextInput
                        style={{
                            width: '100%',
                            borderBottomWidth: 0.5,
                            marginBottom: 5,
                            borderColor: 'gray'
                        }}
                        placeholder={'What do you think about are product?'}
                        multiline={true}
                        onChangeText={text => {
                            let toSubmitData = this.state.toSubmitData;
                            let feedback = text;
                            toSubmitData[index] = {
                                ...toSubmitData[index],
                                feedback: feedback
                            };
                            this.setState({ toSubmitData: toSubmitData });
                        }}
                    />
                </View>
            </View>
        );
    };

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View
                    style={{
                        flex: 0.1,
                        backgroundColor: '#1fa5f2',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                >
                    <Text style={{ fontSize: 18, color: 'white' }}>
                        {this.state.data.orderNo}
                    </Text>
                </View>
                <View style={{ paddingVertical: 10, flex: 0.9 }}>
                    <FlatList
                        data={this.state.data.orders}
                        renderItem={this.renderOrders}
                        keyExtractor={item => item._id}
                    />
                    <TouchableOpacity
                        onPress={() => {
                            console.log(this.state.toSubmitData);
                            axios
                                .put(
                                    `https://pharmakeia.herokuapp.com/order/${this.state.data._id}/feedback`,
                                    {
                                        feedbacks: this.state.toSubmitData
                                    }
                                )
                                .then(res => {
                                    console.log(res);
                                    if (res) {
                                        Alert.alert('Rating Submited', '', [
                                            {
                                                text: 'OK',
                                                onPress: () =>
                                                    this.props.navigation.navigate(
                                                        'ForFeedback'
                                                    )
                                            }
                                        ]);
                                    }
                                })
                                .catch(error => {
                                    console.log(error);
                                });
                        }}
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: '#1fa5f2',
                            padding: 20
                        }}
                    >
                        <Text style={{ color: 'white', fontSize: 14 }}>
                            Submit Rating
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

export default ForFeedbackDetail;
