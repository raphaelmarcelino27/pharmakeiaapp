import React, { Component } from 'react';
import {
    View,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    FlatList
} from 'react-native';
import axios from 'axios';
import { Icon } from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
import { Rating } from 'react-native-ratings';

class ProductDetails extends Component {
    state = { DATA: {} };
    componentDidMount() {
        const { navigation } = this.props;
        const id = navigation.getParam('id', 'NO-ID');
        axios
            .get(`https://pharmakeia.herokuapp.com/product/${id}`)
            .then(res => {
                console.log(res.data);
                this.setState({ DATA: res.data[0] });
            })
            .catch(error => {
                console.log(error.response.data);
            });
    }

    storeData = async obj => {
        //Get the current cart
        let cart = await AsyncStorage.getItem('cart');
        cart = cart ? JSON.parse(cart) : [];

        const index = cart.findIndex(i => i._id === obj._id);

        if (index === -1) {
            cart.push({
                ...obj,
                quantity: 1
            });
        } else {
            cart[index].quantity++;
        }
        AsyncStorage.setItem('cart', JSON.stringify(cart));
    };

    renderFeedbacks = ({ item, index }) => {
        console.log(item, index);
        const { _id, rating, customer, product, feedback } = item;
        return (
            <View
                style={{
                    alignItems: 'flex-start',
                    padding: 10
                }}
            >
                <Rating
                    type="star"
                    ratingCount={5}
                    readonly
                    imageSize={20}
                    showRating={false}
                    startingValue={rating}
                />
                <Text>{feedback}</Text>
            </View>
        );
    };

    render() {
        const { navigation } = this.props;
        const { DATA } = this.state;
        const {
            _id,
            productTitle,
            productDescription,
            price,
            stock,
            isPrescriptionNeeded,
            image,
            productNo,
            isActive,
            ratings,
            feedbacks
        } = this.state.DATA;

        const obj = {
            _id: DATA._id,
            productDescription: DATA.productDescription,
            productTitle: DATA.productTitle,
            productNo: DATA.productNo,
            price: DATA.price
        };
        return (
            <View style={{ flex: 1 }}>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={styles.scrollViewContainer}
                >
                    <View>
                        <Image
                            source={{
                                uri: `http://pharmakeia.herokuapp.com/${DATA.image}`,
                                width: '100%',
                                height: 300
                            }}
                        />
                        <TouchableOpacity
                            style={[
                                styles.goBackButtonTouchableOpacity,
                                { padding: 10 }
                            ]}
                            onPress={() => this.props.navigation.goBack()}
                        >
                            <Icon
                                color="#1fa5f2"
                                name="arrow-left"
                                type="font-awesome"
                                size={25}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[
                                styles.goBackButtonTouchableOpacity,
                                { padding: 10, right: 1 }
                            ]}
                            onPress={() => this.storeData(obj)}
                        >
                            <Icon
                                color="#1fa5f2"
                                name="cart-plus"
                                type="font-awesome"
                                size={30}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={{ padding: 20 }}>
                        <Text
                            style={[
                                styles.productTitle,
                                { fontWeight: 'bold' }
                            ]}
                        >
                            {productTitle}
                        </Text>
                        <Text style={styles.productPrice}>{`₱${
                            price ? price.toFixed(2) : 0
                        }`}</Text>
                        <Text style={styles.productDescriptionHeader}>
                            Description
                        </Text>
                        <Text style={styles.productDescription}>
                            {DATA.productDescription}
                        </Text>
                    </View>
                    <View style={{ paddingHorizontal: 20 }}>
                        <View
                            style={{
                                alignItems: 'flex-start',
                                borderBottomWidth: 0.5,
                                borderColor: 'gray',
                                marginBottom: 20
                            }}
                        >
                            <Text
                                style={[
                                    styles.productDescriptionHeader,
                                    { marginBottom: 10, marginRight: 10 }
                                ]}
                            >
                                Product Ratings
                            </Text>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center'
                                }}
                            >
                                <Rating
                                    style={{ marginRight: 10 }}
                                    type="star"
                                    ratingCount={5}
                                    readonly
                                    imageSize={15}
                                    showRating={false}
                                    startingValue={ratings}
                                />
                                <Text style={{ marginRight: 10 }}>{`${
                                    ratings ? ratings.toFixed(1) : 0
                                }/5`}</Text>
                                <Text>{`(${
                                    feedbacks ? feedbacks.length : 0
                                } Reviews)`}</Text>
                            </View>
                        </View>
                        <FlatList
                            showsVerticalScrollIndicator={false}
                            data={feedbacks}
                            renderItem={this.renderFeedbacks}
                            keyExtractor={item => item._id}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = {
    scrollViewContainer: {},
    goBackButtonTouchableOpacity: {
        position: 'absolute',
        top: 0,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    goBackButtonTouchableOpacityText: {
        fontSize: 17,
        marginLeft: 5
    },
    productTitle: {
        fontSize: 20,
        marginBottom: 5
    },
    productDescriptionHeader: {
        fontWeight: 'bold',
        fontSize: 16,
        marginTop: 10,
        color: 'gray'
    },
    productDescription: {
        fontSize: 16,
        marginTop: 10
    },
    productPrice: {
        color: '#59bfef',
        fontSize: 16
    },
    addToCartTouchableOpacity: {
        alignSelf: 'center',
        backgroundColor: '#59bfef',
        width: '100%',
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20
    },
    addToCartTouchableOpacityText: {
        color: 'white'
    }
};

export default ProductDetails;
