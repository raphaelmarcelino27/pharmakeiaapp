import React, {Component} from 'react';
import {View, Text, ScrollView, Image, TouchableOpacity} from 'react-native';
import axios from 'axios';
import {Icon} from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
class HomeProductDetails extends Component {
  state = {DATA: []};
  componentDidMount() {
    const {navigation} = this.props;
    const id = navigation.getParam('id', 'NO-ID');
    axios
      .get(`https://pharmakeia.herokuapp.com/product/${id}`)
      .then(res => {
        this.setState({DATA: res.data});
      })
      .catch(error => {
        console.log(error.response.data);
      });
  }

  storeData = async obj => {
    //Get the current cart
    let cart = await AsyncStorage.getItem('cart');
    cart = cart ? JSON.parse(cart) : [];

    const index = cart.findIndex(i => i._id === obj._id);

    if (index === -1) {
      cart.push({
        ...obj,
        quantity: 1,
      });
    } else {
      cart[index].quantity++;
    }
    AsyncStorage.setItem('cart', JSON.stringify(cart));
  };

  render() {
    const {navigation} = this.props;
    const {DATA} = this.state;
    const obj = {
      _id: DATA._id,
      productDescription: DATA.productDescription,
      productTitle: DATA.productTitle,
      productNo: DATA.productNo,
      price: DATA.price,
    };
    return (
      <View style={{flex: 1}}>
        <ScrollView contentContainerStyle={styles.scrollViewContainer}>
          <Image
            source={{
              uri: `http://pharmakeia.herokuapp.com/${DATA.image}`,
              width: '100%',
              height: 300,
            }}
          />
          <TouchableOpacity
            style={styles.goBackButtonTouchableOpacity}
            onPress={() => this.props.navigation.goBack()}>
            <Icon name="arrow-left" type="font-awesome" size={17} />
            <Text style={styles.goBackButtonTouchableOpacityText}>Back</Text>
          </TouchableOpacity>
          <Text style={styles.productTitle}>{DATA.productTitle}</Text>
          <Text style={styles.productPrice}>₱{DATA.price}</Text>
          <Text style={styles.productDescriptionHeader}>Description</Text>
          <Text style={styles.productDescription}>
            {DATA.productDescription}
          </Text>
          <TouchableOpacity
            onPress={() => this.storeData(obj)}
            style={styles.addToCartTouchableOpacity}>
            <Text style={styles.addToCartTouchableOpacityText}>
              Add To Cart
            </Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}

const styles = {
  scrollViewContainer: {
    padding: 20,
  },
  goBackButtonTouchableOpacity: {
    position: 'absolute',
    top: 0,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  goBackButtonTouchableOpacityText: {
    fontSize: 17,
    marginLeft: 5,
  },
  productTitle: {
    fontSize: 20,
  },
  productDescriptionHeader: {
    fontWeight: 'bold',
    fontSize: 16,
    marginTop: 10,
  },
  productDescription: {
    fontFamily: 'Roboto-Light',
    fontSize: 16,
    marginTop: 10,
  },
  productPrice: {
    color: '#59bfef',
    fontSize: 18,
    marginTop: 10,
  },
  addToCartTouchableOpacity: {
    alignSelf: 'center',
    backgroundColor: '#59bfef',
    width: '100%',
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
  addToCartTouchableOpacityText: {
    color: 'white',
  },
};

export default HomeProductDetails;
