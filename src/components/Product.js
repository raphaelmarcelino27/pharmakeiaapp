import React, { Component } from 'react';
import {
    View,
    Text,
    ScrollView,
    FlatList,
    TouchableOpacity,
    Image,
    Alert,
    ActivityIndicator
} from 'react-native';
import { Icon } from 'react-native-elements';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import { Rating } from 'react-native-ratings';

class Product extends Component {
    state = { DATA: [], currentCart: [] };

    componentDidMount() {
        axios
            .get(`https://pharmakeia.herokuapp.com/product`)
            .then(res => {
                console.log(res);
                this.setState({ DATA: res.data });
            })
            .catch(error => {
                console.log(error);
                Alert.alert(error);
            });
    }

    storeData = async obj => {
        //Get the current cart
        let cart = await AsyncStorage.getItem('cart');
        cart = cart ? JSON.parse(cart) : [];
        console.log(cart);
        const index = cart.findIndex(i => i._id === obj._id);
        if (index === -1) {
            cart.push({
                ...obj,
                quantity: 1
            });
        } else {
            cart[index].quantity++;
        }
        AsyncStorage.setItem('cart', JSON.stringify(cart));

        let newcart = await AsyncStorage.getItem('cart');
        newcart = JSON.parse(newcart);
        console.log(newcart);
    };

    renderItem = ({ item }) => {
        console.log(item);
        const obj = {
            _id: item._id,
            productDescription: item.productDescription,
            productTitle: item.productTitle,
            productNo: item.productNo,
            price: item.price,
            image: item.image,
            isPrescriptionNeeded: item.isPrescriptionNeeded
                ? item.isPrescriptionNeeded
                : false
        };
        return (
            <TouchableOpacity
                onPress={() =>
                    this.props.navigation.navigate('ProductDetails', {
                        id: item._id
                    })
                }
            >
                <View style={styles.productContainer}>
                    <View style={styles.productImageContainer}>
                        <Image
                            source={{
                                uri: `https://pharmakeia.herokuapp.com/${
                                    item.image
                                }?${Date.now()}`
                            }}
                            style={styles.productImage}
                        />
                    </View>
                    <View style={[styles.productDetailsContainer]}>
                        <Text
                            style={[
                                styles.productTitle,
                                { fontSize: 17, marginBottom: 7 }
                            ]}
                        >
                            {item.productTitle.substring(0, 15)}
                        </Text>
                        <View
                            style={{
                                alignItems: 'flex-start',
                                marginBottom: 7
                            }}
                        >
                            <Rating
                                type="star"
                                ratingCount={5}
                                readonly
                                imageSize={15}
                                showRating={false}
                                startingValue={item.ratings}
                            />
                        </View>

                        <Text
                            style={[
                                styles.productDescription,
                                { color: 'gray', fontSize: 15, marginBottom: 5 }
                            ]}
                        >
                            {item.productNo}
                        </Text>
                        <Text
                            style={[
                                styles.price,
                                { fontSize: 15, marginBottom: 5 }
                            ]}
                        >
                            ₱ {item.price.toFixed(2)}
                        </Text>
                    </View>
                    <View style={styles.addToCartContainer}>
                        <TouchableOpacity onPress={() => this.storeData(obj)}>
                            <View style={styles.addToCartContentContainer}>
                                <Text style={styles.addToCartContentText}>
                                    ADD TO CART
                                </Text>
                                <Icon name="cart-plus" type="font-awesome" />
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </TouchableOpacity>
        );
    };

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.headerContainer}>
                    <Text style={styles.headerText}>Products</Text>
                </View>
                <View style={styles.contentContainer}>
                    {this.state.DATA.length === 0 ? (
                        <View style={styles.loadingContainer}>
                            <ActivityIndicator size="large" color="#0abbce" />
                        </View>
                    ) : (
                        <FlatList
                            showsVerticalScrollIndicator={false}
                            data={this.state.DATA}
                            renderItem={this.renderItem}
                            keyExtractor={item => item._id}
                        />
                    )}
                </View>
            </View>
        );
    }
}

const styles = {
    headerContainer: {
        height: 50,
        backgroundColor: '#1fa5f2',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerText: {
        fontSize: 20,
        color: 'white'
    },
    contentContainer: {
        flex: 1
    },
    loadingContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    productContainer: {
        height: 120,
        marginBottom: 20,
        flexDirection: 'row'
    },
    productImageContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    productImage: {
        height: 100,
        width: 100
    },
    productDetailsContainer: {
        flex: 1,
        justifyContent: 'center',
        paddingLeft: 10
    },
    productTitle: {
        fontWeight: 'bold'
    },
    productDescription: {
        fontSize: 12
    },
    productPrice: {
        fontSize: 12
    },
    addToCartContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    addToCartContentContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    addToCartContentText: {
        fontWeight: 'bold'
    }
};
export default Product;
