import React, {Component} from 'react';
import {
  SafeAreaView,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  View,
  ActivityIndicator,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';

class Login extends Component {
  state = {email: '', password: '', buttonLoading: false, buttonDisable: false};
  componentDidMount() {
    AsyncStorage.clear();
  }
  onPress = () => {
    this.setState({
      buttonLoading: true,
      buttonDisable: true,
    });
    if (this.state.password.length == 0 || this.state.email.length == 0) {
      alert('Please fill the required Fields!');
      this.setState({
        buttonLoading: false,
        buttonDisable: false,
      });
    } else {
      axios
        .post('https://pharmakeia.herokuapp.com/customer/auth', {
          email: this.state.email,
          password: this.state.password,
        })
        .then(async res => {
          let customerData = res.data[0];
          customerData = JSON.stringify(customerData);
          AsyncStorage.setItem('customerData', customerData);
          let test = await AsyncStorage.getItem('customerData');
          test = JSON.parse(test);
          this.setState({
            email: '',
            password: '',
            buttonLoading: false,
            buttonDisable: true,
          });
          this.props.navigation.navigate('InsideNavigator');
        })
        .catch(error => {
          console.log(error);
          console.log(error.response.data);
          if (error.response.data == 'Invalid login.') {
            alert("Account doesn't exist!");
          }
          this.setState({
            email: '',
            password: '',
            buttonLoading: false,
            buttonDisable: false,
          });
        });
    }
  };

  buttonLoading = () => {
    if (this.state.buttonLoading == true) {
      return <ActivityIndicator size="small" color="white" />;
    } else {
      return <Text style={{color: 'white'}}>Login</Text>;
    }
  };
  render() {
    return (
      <KeyboardAvoidingView
        behavior="padding"
        style={{
          height: '100%',
          width: '100%',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
          }}>
          <View style={{height: 300, width: 300, margin: 0, padding: 0}}>
            <Image
              source={require('../../image/Logo.png')}
              style={{width: '100%', height: '100%'}}
            />
          </View>
          <TextInput
            style={{
              borderBottomWidth: 1,
              borderColor: '#1b6bb9',
              width: '80%',
              textAlign: 'center',
              color: 'steelblue',
              margin: 0,
              padding: 0,
            }}
            underlineColorAndroid="rgba(0,0,0,0)"
            placeholder="Email"
            placeholderTextColor="#1b6bb9"
            value={this.state.email}
            onChangeText={email => this.setState({email})}
          />
          <TextInput
            secureTextEntry
            style={{
              borderBottomWidth: 1,
              borderColor: '#1b6bb9',
              width: '80%',
              textAlign: 'center',
              color: 'steelblue',
              marginTop: 50,
              margin: 0,
              padding: 0,
            }}
            underlineColorAndroid="rgba(0,0,0,0)"
            placeholder="Password"
            placeholderTextColor="#1b6bb9"
            value={this.state.password}
            onChangeText={password => this.setState({password})}
          />
          <TouchableOpacity
            style={{
              backgroundColor: '#59bfef',
              height: 40,
              borderRadius: 12,
              width: '80%',
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 50,
            }}
            onPress={this.onPress}
            disabled={this.state.buttonDisable}>
            {this.buttonLoading()}
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Register')}>
            <Text style={{color: '#27c', marginTop: 20}}>Create an account?</Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text style={{color: '#27c', marginTop: 20, marginBottom: 20}}>
              Terms of Use
            </Text>
          </TouchableOpacity>
        </View>
        <View />
      </KeyboardAvoidingView>
    );
  }
}

export default Login;
