import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';

class UploadPrescription extends Component {
    state = { imageFrom: '' };
    handleChoosePhoto = async () => {
        const options = {};

        this.setState({ imageFrom: 'gallery' });
        ImagePicker.launchImageLibrary(options, response => {
            delete response.fileSize;
            delete response.isVertical;
            delete response.height;
            delete response.width;
            delete response.data;
            delete response.path;
            delete response.originalRotation;
            delete response.timestamp;
            console.log(response);
            if (response.uri) {
                alert('SuccessFully Uploaded');
                this.props.navigation.navigate('ConfirmUploadPrescription', {
                    prescriptionImage: response
                });
            }
        });
    };

    openCamera = () => {
        this.setState({ imageFrom: 'camera' });
        const options = {};
        ImagePicker.launchCamera(options, response => {
            console.log(response);
            delete response.fileSize;
            delete response.isVertical;
            delete response.height;
            delete response.width;
            delete response.data;
            delete response.path;
            delete response.originalRotation;
            delete response.timestamp;
            console.log(response);
            if (response.uri) {
                alert('SuccessFully Uploaded');
                this.props.navigation.navigate('ConfirmUploadPrescription', {
                    prescriptionImage: response
                });
            }
        });
    };

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'whitesmoke' }}>
                <View
                    style={{
                        flex: 0.1,
                        backgroundColor: '#1fa5f2',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                >
                    <Text style={{ color: 'white', fontSize: 18 }}>
                        Upload Prescription
                    </Text>
                </View>
                <View style={{ flex: 0.9 }}>
                    <TouchableOpacity
                        onPress={async () => {
                            AsyncStorage.setItem('imageFrom', 'camera');
                            this.openCamera();
                        }}
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderBottomWidth: 1
                        }}
                    >
                        <Icon name="camera" type="font-awesome" size={100} />
                        <Text style={{ fontSize: 20 }}>Take a Photo</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={async () => {
                            AsyncStorage.setItem('imageFrom', 'gallery');
                            this.handleChoosePhoto();
                        }}
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderTopWidth: 1
                        }}
                    >
                        <Icon name="photo" type="font-awesome" size={100} />
                        <Text style={{ fontSize: 20 }}>
                            Choose From Gallery
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

export default UploadPrescription;
