import React, { Component } from 'react';
import { SafeAreaView, Text, Image, TextInput, TouchableOpacity, KeyboardAvoidingView, View } from 'react-native';
import { LoginManager, LoginButton, AccessToken } from 'react-native-fbsdk'

class MainLogin extends Component {

    state = { email: '', password: '' }

    onPress = () => {
        alert(this.state.email)
    }


    loginFacebook = () => {
        LoginManager.logOut(function (res) {
            console.log(res)
        })
        LoginManager.logInWithPermissions(["public_profile"])
            .then(
                function (result) {
                    if (result.isCancelled) {
                        console.log("Login cancelled");
                    }
                    else {
                        console.log("Login success with permissions: " + result.grantedPermissions.toString());
                        this.props.navigation.navigate('Login')
                    }
                },
                function (error) {
                    console.log("Login fail with error: " + error);
                }
            );
    }

    render() {
        return (
            <KeyboardAvoidingView behavior="padding" style={{ height: '100%', width: '100%', alignItems: 'center', justifyContent: 'center' }}>
                <View style={{ alignItems: 'center', justifyContent: 'center', width: '100%' }}>
                    <View style={{ height: 300, width: 300, margin: 0, padding: 0 }}>
                        <Image
                            source={require('../../image/Logo.png')}
                            style={{ width: '100%', height: '100%' }} />
                    </View>
                    <TouchableOpacity
                        style={{ backgroundColor: '#59bfef', height: 40, borderRadius: 12, width: '80%', justifyContent: 'center', alignItems: 'center' }}
                        onPress={() => this.props.navigation.navigate('Login')}
                    >
                        <Text style={{ color: 'white' }}>
                            Login with Email
                        </Text>
                    </TouchableOpacity>
                    <Text style={{ width: '80%', borderBottomWidth: 1, borderColor: '#1b6bb9' }}>

                    </Text>
                    <TouchableOpacity
                        style={{ backgroundColor: '#3a54a1', height: 40, borderRadius: 12, width: '80%', justifyContent: 'center', alignItems: 'center', marginTop: 20 }}
                        onPress={this.loginFacebook}
                    >
                        <Text style={{ color: 'white' }}>
                            Login with Facebook
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{ backgroundColor: '#de4b39', height: 40, borderRadius: 12, width: '80%', justifyContent: 'center', alignItems: 'center', marginTop: 20 }}
                        onPress={this.onPress}
                    >
                        <Text style={{ color: 'white' }}>
                            Login with Google Account
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('Register')}
                    >
                        <Text style={{ color: '#27c', marginTop: 20 }}>New Here? Create an account</Text>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text style={{ color: '#27c', marginTop: 20, marginBottom: 20 }}>Terms of Use</Text>
                    </TouchableOpacity>
                </View>
                <View>

                </View>
            </KeyboardAvoidingView>
        )
    }
}

export default MainLogin;