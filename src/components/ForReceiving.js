import React, { Component } from 'react';
import { View, Text, FlatList, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import moment from 'moment';
import { TouchableOpacity } from 'react-native-gesture-handler';

class ForReceiving extends Component {
    state = { id: '', ForReceiving: [] };
    componentDidMount = () => {
        this.setState({ ForReceiving: this.props.navigation.state.params });
    };

    renderItem = ({ item, index }) => {
        console.log(item);
        const { orderNo, dateCreated, payment } = item;
        return (
            <TouchableOpacity
                key={item._id}
                onPress={() => {
                    this.props.navigation.navigate('ForReceivingDetail', {
                        item
                    });
                }}
                style={{
                    backgroundColor: 'white',
                    shadowOffset: { width: 0, height: 0 },
                    shadowColor: 'black',
                    shadowOpacity: 0.2,
                    elevation: 3,
                    padding: 20,
                    marginBottom: 5,
                    justifyContent: 'center'
                }}
            >
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between'
                    }}
                >
                    <Text
                        style={{
                            fontSize: 15,
                            fontWeight: 'bold',
                            marginRight: 20
                        }}
                    >
                        Order No:
                    </Text>
                    <Text style={{ fontSize: 15 }}>{orderNo}</Text>
                </View>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between'
                    }}
                >
                    <Text
                        style={{
                            fontSize: 15,
                            fontWeight: 'bold',
                            marginRight: 20
                        }}
                    >
                        Date Ordered:
                    </Text>
                    <Text style={{ fontSize: 15 }}>
                        {moment(dateCreated).format('MMMM Do YYYY, h:mm:ss a')}
                    </Text>
                </View>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between'
                    }}
                >
                    <Text
                        style={{
                            fontSize: 15,
                            fontWeight: 'bold',
                            marginRight: 20
                        }}
                    >
                        Mode of payment:
                    </Text>
                    <Text style={{ fontSize: 15 }}>
                        {payment.toUpperCase()}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    };

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View
                    style={{
                        flex: 0.1,
                        backgroundColor: '#1fa5f2',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                >
                    <Text style={{ fontSize: 18, color: 'white' }}>
                        For Receiving
                    </Text>
                </View>
                <View style={{ flex: 0.9 }}>
                    {this.state.ForReceiving.length != 0 ? (
                        <FlatList
                            showsVerticalScrollIndicator={false}
                            data={this.state.ForReceiving}
                            renderItem={this.renderItem}
                            keyExtractor={item => item._id}
                        />
                    ) : (
                        <ActivityIndicator
                            size="large"
                            color="#0abbce"
                            style={{ flex: 1, alignSelf: 'center' }}
                        />
                    )}
                </View>
            </View>
        );
    }
}

export default ForReceiving;
