import React, { Component } from 'react';
import {
    View,
    Text,
    TextInput,
    ScrollView,
    Alert,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationEvents } from 'react-navigation';

class AccountSettings extends Component {
    state = {
        customerData: [],
        firstName: '',
        lastName: '',
        email: '',
        contactNo: '',
        address: '',
        _id: '',
        loading: false,
        buttonDisable: false
    };
    componentDidMount = async () => {
        let customerData = await AsyncStorage.getItem('customerData');
        customerData = JSON.parse(customerData);

        console.log('Line 22 Customer Data', customerData);
        axios
            .get(`http://pharmakeia.herokuapp.com/customer/${customerData._id}`)
            .then(res => {
                console.log('Line 26:', res);
                this.setState({ _id: res.data._id });
                this.setState({ firstName: res.data.firstName });
                this.setState({ lastName: res.data.lastName });
                this.setState({ email: res.data.email });
                this.setState({ contactNo: res.data.contactNo });
                this.setState({ address: res.data.address });
            })
            .catch(error => {
                console.log(error);
            });
    };

    getCustomerData = async () => {
        let customerData = await AsyncStorage.getItem('customerData');
        customerData = JSON.parse(customerData);

        console.log(customerData._id);

        axios
            .get(`http://pharmakeia.herokuapp.com/customer/${customerData._id}`)
            .then(res => {
                this.setState({ _id: res.data._id });
                this.setState({ firstName: res.data.firstName });
                this.setState({ lastName: res.data.lastName });
                this.setState({ email: res.data.email });
                this.setState({ contactNo: res.data.contactNo });
                this.setState({ address: res.data.address });
            })
            .catch(error => {
                console.log(error);
            });
    };

    render() {
        return (
            <View style={{ flex: 1 }}>
                <NavigationEvents onWillFocus={() => this.getCustomerData()} />
                <View
                    style={{
                        flex: 0.1,
                        backgroundColor: '#1fa5f2',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                >
                    <Text style={{ fontSize: 20, color: 'white' }}>
                        Account Settings
                    </Text>
                </View>
                <View style={{ flex: 0.9, padding: 30 }}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View>
                            <Text style={{ fontSize: 16, color: 'gray' }}>
                                First Name
                            </Text>
                            <TextInput
                                value={this.state.firstName}
                                style={{
                                    width: '100%',
                                    borderBottomWidth: 0.5,
                                    borderColor: 'gray',
                                    padding: 0,
                                    fontSize: 16
                                }}
                                onChangeText={text =>
                                    this.setState({ firstName: text })
                                }
                            />
                        </View>
                        <View style={{ marginTop: 15 }}>
                            <Text style={{ fontSize: 16, color: 'gray' }}>
                                Last Name
                            </Text>
                            <TextInput
                                value={this.state.lastName}
                                style={{
                                    width: '100%',
                                    borderBottomWidth: 0.5,
                                    borderColor: 'gray',
                                    padding: 0,
                                    fontSize: 16
                                }}
                                onChangeText={text =>
                                    this.setState({ lastName: text })
                                }
                            />
                        </View>
                        <View style={{ marginTop: 15 }}>
                            <Text style={{ fontSize: 16, color: 'gray' }}>
                                Email
                            </Text>
                            <TextInput
                                value={this.state.email}
                                style={{
                                    width: '100%',
                                    borderBottomWidth: 0.5,
                                    borderColor: 'gray',
                                    padding: 0,
                                    fontSize: 16
                                }}
                                onChangeText={text =>
                                    this.setState({ email: text })
                                }
                            />
                        </View>
                        <View style={{ marginTop: 15 }}>
                            <Text style={{ fontSize: 16, color: 'gray' }}>
                                Contact No
                            </Text>
                            <TextInput
                                value={this.state.contactNo}
                                style={{
                                    width: '100%',
                                    borderBottomWidth: 0.5,
                                    borderColor: 'gray',
                                    padding: 0,
                                    fontSize: 16
                                }}
                                onChangeText={text =>
                                    this.setState({ contactNo: text })
                                }
                            />
                        </View>
                        <View style={{ marginTop: 15 }}>
                            <Text style={{ fontSize: 16, color: 'gray' }}>
                                Address
                            </Text>
                            <TextInput
                                value={this.state.address}
                                style={{
                                    width: '100%',
                                    borderBottomWidth: 0.5,
                                    borderColor: 'gray',
                                    padding: 0,
                                    fontSize: 16
                                }}
                                onChangeText={text =>
                                    this.setState({ address: text })
                                }
                            />
                        </View>
                        <View style={{ marginTop: 15 }}>
                            <TouchableOpacity
                                disabled={this.state.buttonDisable}
                                onPress={() => {
                                    this.setState({
                                        loading: true,
                                        buttonDisable: true
                                    });
                                    if (
                                        this.state.firstName &&
                                        this.state.lastName &&
                                        this.state.email &&
                                        this.state.contactNo &&
                                        this.state.address
                                    ) {
                                        axios
                                            .put(
                                                `http://pharmakeia.herokuapp.com/customer/${this.state._id}`,
                                                {
                                                    firstName: this.state
                                                        .firstName,
                                                    lastName: this.state
                                                        .lastName,
                                                    email: this.state.email,
                                                    contactNo: this.state
                                                        .contactNo,
                                                    address: this.state.address
                                                }
                                            )
                                            .then(res => {
                                                setTimeout(() => {
                                                    this.setState({
                                                        loading: false,
                                                        buttonDisable: false
                                                    });
                                                    Alert.alert(
                                                        'Account updated'
                                                    );
                                                }, 1000);
                                            })
                                            .catch(error => {
                                                console.log(error);
                                            });
                                    } else {
                                        setTimeout(() => {
                                            Alert.alert(
                                                'Please fill all the fields'
                                            );
                                            this.setState({
                                                loading: false,
                                                buttonDisable: false
                                            });
                                        }, 1000);
                                    }
                                }}
                                style={{
                                    backgroundColor: '#1fa5f2',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    height: 30,
                                    borderRadius: 15,
                                    width: '50%',
                                    alignSelf: 'center'
                                }}
                            >
                                {this.state.loading ? (
                                    <ActivityIndicator
                                        size="small"
                                        color="white"
                                    />
                                ) : (
                                    <Text style={{ color: 'white' }}>Save</Text>
                                )}
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
            </View>
        );
    }
}

export default AccountSettings;
