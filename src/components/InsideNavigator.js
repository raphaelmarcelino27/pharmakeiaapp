import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Icon } from 'react-native-elements';
import {
    createBottomTabNavigator,
    createStackNavigator
} from 'react-navigation';
import Product from './Product';
import Profile from './Profile';
import Cart from './Cart';
import Home from './Home';
import ProductDetails from './ProductDetails';
import Shipping from '../components/checkout/Shipping';
import Payment from '../components/checkout/Payment';
import Confirm from '../components/checkout/Confirm';
import CreditCard from './checkout/CreditCard';
import AccountSettings from './profile/AccountSettings';
import UploadPrescription from './UploadPrescription';
import ConfirmUploadPrescription from './ConfirmUploadPrescription';
import HomeProducts from './HomeProducts';
import ChangePassword from './ChangePassword';
import HomeProductDetails from './HomeProductDetails';
import ForApproval from './ForApproval';
import ForReceiving from './ForReceiving';
import ForApprovalDetail from './ForApprovalDetail';
import ForFeedback from './ForFeedback';
import ForFeedbackDetail from './ForFeedbackDetail';
import ForReceivingDetail from './ForReceivingDetail';
const ProductNavigator = createStackNavigator(
    {
        Product: { screen: Product },
        ProductDetails: { screen: ProductDetails }
    },
    {
        initialRouteName: 'Product',
        headerMode: 'none',
        transitionConfig: () => ({
            screenInterpolator: sceneProps => {
                const { layout, position, scene } = sceneProps;
                const { index } = scene;
                const width = layout.initWidth;

                return {
                    opacity: position.interpolate({
                        inputRange: [index - 1, index, index + 1],
                        outputRange: [0, 1, 0]
                    }),
                    transform: [
                        {
                            translateX: position.interpolate({
                                inputRange: [index - 1, index, index + 1],
                                outputRange: [-width, 0, width]
                            })
                        }
                    ]
                };
            }
        })
    }
);

const CartNavigator = createStackNavigator(
    {
        Cart: { screen: Cart },
        Shipping: { screen: Shipping },
        Payment: { screen: Payment },
        Confirm: { screen: Confirm },
        CreditCard: { screen: CreditCard },
        UploadPrescription: { screen: UploadPrescription },
        ConfirmUploadPrescription: { screen: ConfirmUploadPrescription }
    },
    {
        initialRouteName: 'Cart',
        headerMode: 'none',
        transitionConfig: () => ({
            screenInterpolator: sceneProps => {
                const { layout, position, scene } = sceneProps;
                const { index } = scene;
                const width = layout.initWidth;

                return {
                    opacity: position.interpolate({
                        inputRange: [index - 1, index, index + 1],
                        outputRange: [0, 1, 0]
                    }),
                    transform: [
                        {
                            translateX: position.interpolate({
                                inputRange: [index - 1, index, index + 1],
                                outputRange: [-width, 0, width]
                            })
                        }
                    ]
                };
            }
        })
    }
);

const ProfileNavigator = createStackNavigator(
    {
        Profile: { screen: Profile },
        AccountSettings: { screen: AccountSettings },
        ChangePassword: { screen: ChangePassword },
        ForApproval: { screen: ForApproval },
        ForReceiving: { screen: ForReceiving },
        ForApprovalDetail: { screen: ForApprovalDetail },
        ForFeedback: { screen: ForFeedback },
        ForFeedbackDetail: { screen: ForFeedbackDetail },
        ForReceivingDetail: { screen: ForReceivingDetail }
    },
    {
        initialRouteName: 'Profile',
        headerMode: 'none',
        transitionConfig: () => ({
            screenInterpolator: sceneProps => {
                const { layout, position, scene } = sceneProps;
                const { index } = scene;
                const width = layout.initWidth;

                return {
                    opacity: position.interpolate({
                        inputRange: [index - 1, index, index + 1],
                        outputRange: [0, 1, 0]
                    }),
                    transform: [
                        {
                            translateX: position.interpolate({
                                inputRange: [index - 1, index, index + 1],
                                outputRange: [-width, 0, width]
                            })
                        }
                    ]
                };
            }
        })
    }
);

const HomeNavigator = createStackNavigator(
    {
        Home: { screen: Home },
        HomeProducts: { screen: HomeProducts },
        HomeProductDetails: { screen: HomeProductDetails }
    },
    {
        initialRouteName: 'Home',
        headerMode: 'none',
        transitionConfig: () => ({
            screenInterpolator: sceneProps => {
                const { layout, position, scene } = sceneProps;
                const { index } = scene;
                const width = layout.initWidth;

                return {
                    opacity: position.interpolate({
                        inputRange: [index - 1, index, index + 1],
                        outputRange: [0, 1, 0]
                    }),
                    transform: [
                        {
                            translateX: position.interpolate({
                                inputRange: [index - 1, index, index + 1],
                                outputRange: [-width, 0, width]
                            })
                        }
                    ]
                };
            }
        })
    }
);

const TabNavigator = createBottomTabNavigator(
    {
        Home: {
            screen: HomeNavigator,
            navigationOptions: () => ({
                tabBarIcon: ({ tintColor }) => (
                    <Icon
                        name="home"
                        type="font-awesome"
                        size={23}
                        color={tintColor}
                    />
                )
            })
        },
        Product: {
            screen: ProductNavigator,
            navigationOptions: () => ({
                tabBarIcon: ({ tintColor }) => (
                    <Icon
                        name="star"
                        type="font-awesome"
                        size={23}
                        color={tintColor}
                    />
                )
            })
        },
        Cart: {
            screen: CartNavigator,
            navigationOptions: () => ({
                tabBarIcon: ({ tintColor }) => (
                    <Icon
                        name="shopping-cart"
                        type="font-awesome"
                        size={23}
                        color={tintColor}
                    />
                )
            })
        },
        Profile: {
            screen: ProfileNavigator,
            navigationOptions: () => ({
                tabBarIcon: ({ tintColor }) => (
                    <Icon
                        name="user"
                        type="font-awesome"
                        size={23}
                        color={tintColor}
                    />
                )
            })
        }
    },
    {
        initialRouteName: 'Profile',
        tabBarOptions: {
            style: {
                paddingTop: 5,
                alignItems: 'center',
                justifyContent: 'center'
            },
            activeTintColor: 'steelblue',
            labelStyle: {
                fontSize: 13,
                paddingBottom: 3,
                paddingTop: 5
            },
            showIcon: true
        }
    }
);

export default TabNavigator;
