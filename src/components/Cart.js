import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    TouchableOpacity,
    Alert,
    ActivityIndicator
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import { Icon } from 'react-native-elements';
import { NavigationEvents } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';

class Cart extends Component {
    state = {
        cart: [],
        totalPrice: 0,
        buttonMinusDisable: false,
        orderCount: 0,
        isPrescriptionNeeded: false,
        prescriptionPhoto: null,
        loading: false,
        buttonDisable: false
    };

    getCart = async () => {
        this.setState({ isPrescriptionNeeded: false });

        let cart = await AsyncStorage.getItem('cart');
        cart = JSON.parse(cart);
        this.setState({ cart: cart });

        this.setState({ orderCount: cart.length });

        if (this.state.cart.length != 0) {
            this.computeTotal();
        } else {
            this.setState({ totalPrice: 0 });
        }

        let isPrescriptionNeeded = false;

        let prescriptionImage = await AsyncStorage.getItem('prescriptionImage');
        prescriptionImage = JSON.parse(prescriptionImage);

        if (prescriptionImage) {
            this.setState({ isPrescriptionNeeded: false });
        } else {
            let BreakException = {};
            this.state.cart.forEach(i => {
                if (i.isPrescriptionNeeded) {
                    this.setState({ isPrescriptionNeeded: true });
                    throw BreakException;
                } else {
                    this.setState({ isPrescriptionNeeded: false });
                }
            });
        }
    };

    computeTotal = () => {
        let totalPrice = 0;
        this.state.cart.map((x, index) => {
            totalPrice += x.quantity * x.price;
        });
        totalPrice = totalPrice.toFixed(2);
        this.setState({ totalPrice: totalPrice });
    };

    addItem = id => {
        const index = this.state.cart.findIndex(i => i._id === id);
        let newCart = this.state.cart;
        newCart[index].quantity++;
        console.log(newCart[index].quantity);
        this.setState({ cart: newCart });
        AsyncStorage.setItem('cart', JSON.stringify(this.state.cart));
        this.computeTotal();
    };

    minusItem = id => {
        const index = this.state.cart.findIndex(i => i._id === id);
        let newCart = this.state.cart;
        newCart[index].quantity--;
        console.log(newCart[index].quantity);
        this.setState({ cart: newCart });
        AsyncStorage.setItem('cart', JSON.stringify(this.state.cart));
        this.computeTotal();
    };

    renderCart = () => {
        if (this.state.cart) {
            return this.state.cart.map((x, index) => {
                let buttonMinusDisable = false;
                x.quantity == 1
                    ? (buttonMinusDisable = true)
                    : (buttonMinusDisable = false);
                return (
                    <View
                        key={x._id}
                        style={{ flexDirection: 'row', height: 150 }}
                    >
                        <View
                            style={{
                                flex: 1,
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}
                        >
                            <Image
                                source={{
                                    uri: `https://pharmakeia.herokuapp.com/${
                                        x.image
                                    }?${Date.now()}`
                                }}
                                style={{
                                    height: 100,
                                    width: 100,
                                    borderRadius: 50
                                }}
                            />
                        </View>
                        <View style={{ flex: 1, justifyContent: 'center' }}>
                            <View style={{ height: 80 }}>
                                <Text
                                    style={{ fontWeight: 'bold', fontSize: 16 }}
                                >
                                    {x.productTitle.substring(0, 15)}
                                </Text>
                                <Text style={{ fontSize: 12 }}>
                                    {x.productDescription.substring(0, 70)}
                                </Text>
                                <Text style={{ fontSize: 11 }}>
                                    {x.isPrescriptionNeeded
                                        ? 'Prescription: Required'
                                        : 'Prescription: Not Required'}
                                </Text>
                            </View>
                        </View>
                        <View
                            style={{
                                flex: 1,
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}
                        >
                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity
                                    disabled={buttonMinusDisable}
                                    onPress={() => this.minusItem(x._id)}
                                    style={{
                                        backgroundColor: '#f05086',
                                        width: 40,
                                        height: 40,
                                        borderRadius: 30,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}
                                >
                                    <Icon
                                        name="minus"
                                        type="font-awesome"
                                        size={18}
                                        color="white"
                                    />
                                </TouchableOpacity>
                                <Text style={{ fontSize: 25, marginLeft: 10 }}>
                                    {x.quantity}
                                </Text>
                                <TouchableOpacity
                                    onPress={() => this.addItem(x._id)}
                                    style={{
                                        marginLeft: 10,
                                        backgroundColor: '#f05086',
                                        width: 40,
                                        height: 40,
                                        borderRadius: 30,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}
                                >
                                    <Icon
                                        name="plus"
                                        type="font-awesome"
                                        size={18}
                                        color="white"
                                    />
                                </TouchableOpacity>
                            </View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}
                            >
                                <Text
                                    style={{
                                        alignSelf: 'flex-end',
                                        paddingRight: 10,
                                        paddingTop: 10,
                                        fontSize: 18,
                                        color: '#58c8b9'
                                    }}
                                >
                                    ₱ {(x.price * x.quantity).toFixed(2)}
                                </Text>
                                <TouchableOpacity
                                    onPress={async () => {
                                        let cart = await AsyncStorage.getItem(
                                            'cart'
                                        );
                                        cart = JSON.parse(cart);
                                        console.log(x._id);
                                        console.log(cart);
                                        cart = cart.filter(i => i._id != x._id);
                                        console.log(cart);
                                        AsyncStorage.setItem(
                                            'cart',
                                            JSON.stringify(cart)
                                        );
                                        this.getCart();
                                    }}
                                >
                                    <Icon
                                        name="trash"
                                        type="font-awesome"
                                        size={20}
                                        color="red"
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                );
            });
        }
    };

    handleChoosePhoto = () => {
        const options = {};
        ImagePicker.launchImageLibrary(options, response => {
            console.log(response);
            if (response.uri) {
                this.setState({ prescriptionPhoto: response });
                alert('SuccessFully Uploaded');
                this.setState({ isPrescriptionNeeded: false });
            }
            console.log('Line 156:', this.state.prescriptionPhoto);
        });
    };

    renderUploadPrescription() {
        return (
            <TouchableOpacity
                onPress={() =>
                    this.props.navigation.navigate('UploadPrescription')
                }
                style={{
                    backgroundColor: '#0abbce',
                    height: 35,
                    borderRadius: 15,
                    width: '100%',
                    alignSelf: 'center',
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
            >
                <Text style={{ color: 'white', fontSize: 18 }}>
                    Upload Prescription
                </Text>
            </TouchableOpacity>
        );
    }

    renderCheckout() {
        return (
            <TouchableOpacity
                disabled={this.state.buttonDisable}
                style={{
                    backgroundColor: '#1fa5f2',
                    height: 35,
                    borderRadius: 15,
                    width: '100%',
                    alignSelf: 'center',
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 10
                }}
                onPress={async () => {
                    if (this.state.totalPrice > 200) {
                        this.setState({
                            loading: true,
                            buttonDisable: true
                        });
                        let cart = await AsyncStorage.getItem('cart');
                        let customerData = await AsyncStorage.getItem(
                            'customerData'
                        );
                        cart = JSON.parse(cart);
                        customerData = JSON.parse(customerData);

                        cart.map(x => {
                            delete x.productDescription;
                            delete x.productTitle;
                            delete x.productNo;
                            x['product'] = x._id;
                            x['total'] = x.quantity * x.price;
                            delete x._id;
                        });

                        let obj = {
                            customer: customerData._id,
                            orderCount: this.state.orderCount,
                            total: this.state.totalPrice,
                            orders: cart
                        };
                        AsyncStorage.setItem(
                            'checkoutOrderData',
                            JSON.stringify(obj)
                        );
                        let checkoutOrderData = await AsyncStorage.getItem(
                            'checkoutOrderData'
                        );
                        console.log(JSON.parse(checkoutOrderData));
                        setTimeout(() => {
                            this.setState({
                                loading: false,
                                buttonDisable: false
                            });
                            this.props.navigation.navigate('Shipping');
                        }, 2000);
                    } else {
                        Alert.alert('Minimum order of 200 pesos');
                    }
                }}
            >
                {this.state.loading ? (
                    <ActivityIndicator size="small" color="white" />
                ) : (
                    <Text style={{ color: 'white', fontSize: 18 }}>
                        Checkout
                    </Text>
                )}
            </TouchableOpacity>
        );
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <NavigationEvents onWillFocus={() => this.getCart()} />
                <View
                    style={{
                        height: 50,
                        backgroundColor: '#1fa5f2',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                >
                    <Text style={{ fontSize: 20, color: 'white' }}>Cart</Text>
                </View>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{
                        flex: 1,
                        borderBottomWidth: 1,
                        borderColor: 'whitesmoke'
                    }}
                >
                    {this.renderCart()}
                </ScrollView>
                <View style={{ flexDirection: 'row', padding: 10 }}>
                    <View style={{ alignItems: 'flex-start', flex: 1 }}>
                        <Text style={{ color: '#1fa5f2', fontSize: 20 }}>
                            SubTotal:
                        </Text>
                    </View>
                    <View style={{ alignItems: 'flex-end', flex: 1 }}>
                        <Text style={{ color: '#1fa5f2', fontSize: 20 }}>
                            ₱ {this.state.totalPrice}
                        </Text>
                    </View>
                </View>
                <View style={{ padding: 10 }}>
                    {this.state.isPrescriptionNeeded
                        ? this.renderUploadPrescription()
                        : this.renderCheckout()}
                </View>
                <NavigationEvents />
            </View>
        );
    }
}

export default Cart;
