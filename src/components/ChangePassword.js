import React, { Component } from 'react';
import {
    View,
    Text,
    TextInput,
    ScrollView,
    Alert,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationEvents } from 'react-navigation';

class ChangePassword extends Component {
    state = {
        password: '',
        changePassword: '',
        _id: '',
        loading: false,
        buttonDisable: false
    };
    componentDidMount = async () => {
        let customerData = await AsyncStorage.getItem('customerData');
        customerData = JSON.parse(customerData);
        this.setState({ _id: customerData._id });
    };

    getCustomerData = async () => {
        let customerData = await AsyncStorage.getItem('customerData');
        customerData = JSON.parse(customerData);

        console.log(customerData._id);
        this.setState({ _id: customerData._id });
    };

    render() {
        return (
            <View style={{ flex: 1 }}>
                <NavigationEvents onWillFocus={() => this.getCustomerData()} />
                <View
                    style={{
                        flex: 0.1,
                        backgroundColor: '#1fa5f2',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                >
                    <Text style={{ fontSize: 20, color: 'white' }}>
                        Account Settings
                    </Text>
                </View>
                <View
                    style={{ flex: 0.9, padding: 30, justifyContent: 'center' }}
                >
                    <View style={{ marginTop: 15 }}>
                        <Text style={{ fontSize: 16, color: 'gray' }}>
                            Password
                        </Text>
                        <TextInput
                            secureTextEntry
                            value={this.state.password}
                            style={{
                                width: '100%',
                                borderBottomWidth: 0.5,
                                borderColor: 'gray',
                                padding: 0,
                                fontSize: 16
                            }}
                            onChangeText={text =>
                                this.setState({ password: text })
                            }
                        />
                    </View>
                    <View style={{ marginTop: 15 }}>
                        <Text style={{ fontSize: 16, color: 'gray' }}>
                            Change Password
                        </Text>
                        <TextInput
                            secureTextEntry
                            value={this.state.changePassword}
                            style={{
                                width: '100%',
                                borderBottomWidth: 0.5,
                                borderColor: 'gray',
                                padding: 0,
                                fontSize: 16
                            }}
                            onChangeText={text =>
                                this.setState({ changePassword: text })
                            }
                        />
                    </View>
                    <View style={{ marginTop: 15 }}>
                        <TouchableOpacity
                            disabled={this.state.buttonDisable}
                            onPress={() => {
                                this.setState({
                                    loading: true,
                                    buttonDisable: true
                                });
                                if (
                                    this.state.password &&
                                    this.state.changePassword
                                ) {
                                    if (
                                        this.state.password ===
                                        this.state.changePassword
                                    ) {
                                        console.log(this.state.password);
                                        console.log(this.state.changePassword);
                                        axios
                                            .put(
                                                `http://pharmakeia.herokuapp.com/customer/${this.state._id}`,
                                                {
                                                    password: this.state
                                                        .password
                                                }
                                            )
                                            .then(res => {
                                                Alert.alert(
                                                    'Password successfully changed'
                                                );
                                                setTimeout(() => {
                                                    this.setState({
                                                        loading: false,
                                                        buttonDisable: false
                                                    });
                                                }, 1000);
                                            })
                                            .catch(error => {
                                                console.log(error);
                                                this.setState({
                                                    loading: false,
                                                    buttonDisable: false
                                                });
                                            });
                                    } else {
                                        setTimeout(() => {
                                            Alert.alert(
                                                'Password does not match'
                                            );
                                            this.setState({
                                                loading: false,
                                                buttonDisable: false
                                            });
                                        }, 1000);
                                    }
                                } else {
                                    setTimeout(() => {
                                        Alert.alert(
                                            'Please Fill the blank Fields'
                                        );
                                        this.setState({
                                            loading: false,
                                            buttonDisable: false
                                        });
                                    }, 1000);
                                }
                            }}
                            style={{
                                backgroundColor: '#1fa5f2',
                                alignItems: 'center',
                                justifyContent: 'center',
                                height: 30,
                                borderRadius: 15,
                                width: '50%',
                                alignSelf: 'center'
                            }}
                        >
                            {this.state.loading ? (
                                <ActivityIndicator size="small" color="white" />
                            ) : (
                                <Text style={{ color: 'white' }}>Save</Text>
                            )}
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

export default ChangePassword;
