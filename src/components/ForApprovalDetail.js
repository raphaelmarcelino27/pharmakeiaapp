import React, { Component } from 'react';
import { View, Text, FlatList, Image } from 'react-native';

class ForApprovalDetail extends Component {
    state = { data: {} };
    componentDidMount = () => {
        this.setState({ data: this.props.navigation.state.params.item });
    };

    renderOrders = ({ item, index }) => {
        console.log(item);
        const {
            product: { productTitle, image, productNo },
            price,
            quantity,
            total
        } = item;
        const totalPrice = price * quantity;
        return (
            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginVertical: 10,
                    shadowOffset: { width: 0, height: 0 },
                    shadowColor: 'black',
                    shadowOpacity: 0.2,
                    elevation: 3,
                    paddingHorizontal: 10
                }}
            >
                <View
                    style={{ justifyContent: 'center', alignItems: 'center' }}
                >
                    <Image
                        style={{ width: 80, height: 80 }}
                        source={{
                            uri: `https://pharmakeia.herokuapp.com/${image}`
                        }}
                    />
                </View>
                <View
                    style={{
                        width: '100%',
                        paddingHorizontal: 10,
                        paddingVertical: 5
                    }}
                >
                    <Text style={{ fontWeight: 'bold', fontSize: 17 }}>
                        {productTitle}
                    </Text>
                    <Text
                        style={{
                            fontWeight: '100',
                            color: 'gray',
                            fontSize: 13
                        }}
                    >
                        {productNo}
                    </Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text>{`₱ ${price.toFixed(2)} `}</Text>
                        <Text>{`(Qty: ${quantity})`}</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text
                            style={{ fontWeight: 'bold', fontSize: 15 }}
                        >{`Total Price: `}</Text>
                        <Text style={{ fontSize: 15 }}>{`₱ ${totalPrice.toFixed(
                            2
                        )}`}</Text>
                    </View>
                </View>
            </View>
        );
    };

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View
                    style={{
                        flex: 0.1,
                        backgroundColor: '#1fa5f2',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                >
                    <Text style={{ fontSize: 18, color: 'white' }}>
                        {this.state.data.orderNo}
                    </Text>
                </View>
                <View style={{ padding: 10, flex: 0.9 }}>
                    <Text style={{ color: 'gray', fontSize: 16 }}>
                        Item List
                    </Text>
                    <FlatList
                        data={this.state.data.orders}
                        renderItem={this.renderOrders}
                        keyExtractor={item => item._id}
                    />
                </View>
            </View>
        );
    }
}

export default ForApprovalDetail;
