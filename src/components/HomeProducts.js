import React, { Component } from 'react';
import axios from 'axios';
import {
    View,
    Text,
    TouchableOpacity,
    Alert,
    FlatList,
    Image,
    ActivityIndicator
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';

import { Icon } from 'react-native-elements';

class HomeProducts extends Component {
    state = { products: [], loading: true };
    componentDidMount = () => {
        axios
            .get(
                `https://pharmakeia.herokuapp.com/product/search/${this.props.navigation.state.params.search}`
            )
            .then(res => {
                console.log(res);
                this.setState({ products: res.data });
            })
            .catch(error => {
                console.log(error);
                Alert.alert(error);
            });
    };

    storeData = async obj => {
        //Get the current cart
        let cart = await AsyncStorage.getItem('cart');
        cart = cart ? JSON.parse(cart) : [];
        console.log(cart);
        const index = cart.findIndex(i => i._id === obj._id);
        if (index === -1) {
            cart.push({
                ...obj,
                quantity: 1
            });
        } else {
            cart[index].quantity++;
        }
        AsyncStorage.setItem('cart', JSON.stringify(cart));

        let newcart = await AsyncStorage.getItem('cart');
        newcart = JSON.parse(newcart);
        console.log(newcart);
    };

    renderItem = ({ item }) => {
        const obj = {
            _id: item._id,
            productDescription: item.productDescription,
            productTitle: item.productTitle,
            productNo: item.productNo,
            price: item.price,
            isPrescriptionNeeded: item.isPrescriptionNeeded
                ? item.isPrescriptionNeeded
                : false
        };
        return (
            <TouchableOpacity
                onPress={() =>
                    this.props.navigation.navigate('HomeProductDetails', {
                        id: item._id
                    })
                }
            >
                <View
                    style={{
                        height: 120,
                        marginBottom: 20,
                        flexDirection: 'row'
                    }}
                >
                    <View
                        style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                    >
                        <Image
                            source={{
                                uri: `https://pharmakeia.herokuapp.com/${item.image}`
                            }}
                            style={{ height: 120, width: 120 }}
                        />
                    </View>
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            paddingLeft: 10
                        }}
                    >
                        <Text style={{ fontWeight: 'bold' }}>
                            {item.productTitle.substring(0, 15)}
                        </Text>
                        <Text style={{ fontSize: 12 }}>
                            {item.productDescription.substring(0, 40)}
                        </Text>
                        <Text style={{ fontSize: 12 }}>
                            ₱ {item.price.toFixed(2)}
                        </Text>
                    </View>
                    <View
                        style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                    >
                        <TouchableOpacity onPress={() => this.storeData(obj)}>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center'
                                }}
                            >
                                <Text style={{ fontWeight: 'bold' }}>
                                    ADD TO CART
                                </Text>
                                <Icon name="cart-plus" type="font-awesome" />
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </TouchableOpacity>
        );
    };

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View
                    style={{
                        flex: 0.1,
                        backgroundColor: '#1fa5f2',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                >
                    <Text style={{ fontSize: 18, color: 'white' }}>
                        Products
                    </Text>
                </View>
                <View
                    style={{
                        flex: 0.9,
                        justifyContent:
                            this.state.products === 0 ? 'center' : 'flex-start'
                    }}
                >
                    {this.state.products.length === 0 ? (
                        <View
                            style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                                flex: 1
                            }}
                        >
                            <ActivityIndicator size="large" color="#1fa5f2" />
                        </View>
                    ) : (
                        <FlatList
                            showsVerticalScrollIndicator={false}
                            data={this.state.products}
                            renderItem={this.renderItem}
                            keyExtractor={item => item._id}
                        />
                    )}
                </View>
            </View>
        );
    }
}

export default HomeProducts;
