import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

class ConfirmUploadPrescription extends Component {
    componentDidMount = async () => {};

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View
                    style={{
                        flex: 0.1,
                        backgroundColor: '#1fa5f2',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                >
                    <Text style={{ fontSize: 18, color: 'white' }}>
                        Upload Prescription
                    </Text>
                </View>
                <View
                    style={{
                        flex: 0.9,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                >
                    <Image
                        source={{
                            uri: this.props.navigation.state.params
                                .prescriptionImage.uri
                        }}
                        style={{
                            height: 400,
                            width: 400,
                            backgroundColor: 'green'
                        }}
                    />
                    <TouchableOpacity
                        onPress={async () => {
                            console.log(
                                '34:',
                                JSON.stringify(
                                    this.props.navigation.state.params
                                        .prescriptionImage
                                )
                            );
                            AsyncStorage.setItem(
                                'prescriptionImage',
                                JSON.stringify(
                                    this.props.navigation.state.params
                                        .prescriptionImage
                                )
                            );
                            let test = await AsyncStorage.getItem(
                                'prescriptionImage'
                            );
                            test = JSON.parse(test);
                            console.log('46:', test);

                            Alert.alert('Saved');

                            this.props.navigation.navigate('Cart');
                        }}
                        style={{
                            height: 30,
                            backgroundColor: '#1fa5f2',
                            justifyContent: 'center',
                            alignItems: 'center',
                            width: '30%',
                            borderRadius: 15,
                            marginTop: 20
                        }}
                    >
                        <Text style={{ color: 'white' }}>Save</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

export default ConfirmUploadPrescription;
