import React, { Component } from 'react';
import {
    View,
    Text,
    ScrollView,
    TextInput,
    KeyboardAvoidingView,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';
import axios from 'axios';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
class Register extends Component {
    state = {
        firstName: '',
        lastName: '',
        email: '',
        contactNumber: '',
        address: '',
        password: '',
        confirmPassword: '',
        buttonLoading: false,
        year: '',
        month: '',
        day: '',
        buttonDisable: false
    };

    validateEmail = email => {
        let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };

    buttonLoading() {
        if (this.state.buttonLoading == false) {
            return <Text style={{ color: 'white' }}>Create</Text>;
        } else {
            return <ActivityIndicator size="small" color="white" />;
        }
    }

    createAccount = () => {
        this.setState({ buttonDisable: true });
        let birthMonth = this.state.month;
        let birthYear = this.state.year;
        let birthDay = this.state.day;

        let currentYear = moment().format('YYYY');
        let currentMonth = moment().format('MM');
        let currentDay = moment().format('DD');

        let validYear = `${currentYear - 18}`;

        let underAge = false;
        let passwordMatch = false;
        let emailValid = false;
        let firstNameEmpty = false;
        let lastNameEmpty = false;
        let emailEmpty = false;
        let addressEmpty = false;
        let contactNumberEmpty = false;
        let passwordEmpty = false;
        let confirmPasswordEmpty = false;

        this.setState({ buttonLoading: true });

        if (this.state.year > validYear) {
            alert('Underage');
            underAge = true;
            this.setState({ buttonLoading: false, buttonDisable: false });
        }

        if (this.state.password === this.state.confirmPassword) {
            passwordMatch = true;
        } else {
            alert("Password doesn't Match");
            this.setState({ buttonLoading: false, buttonDisable: false });
        }

        if (this.validateEmail(this.state.email) == true) {
            emailValid = true;
        } else {
            alert('Email is not valid!');
            this.setState({ buttonLoading: false, buttonDisable: false });
        }

        if (this.state.firstName.length == 0) {
            firstNameEmpty = true;
        }

        if (this.state.lastName.length == 0) {
            lastNameEmpty = true;
        }

        if (this.state.email.length == 0) {
            emailEmpty = true;
        }

        if (this.state.address.length == 0) {
            addressEmpty = true;
        }

        if (this.state.contactNumber.length == 0) {
            contactNumberEmpty = true;
        }

        if (this.state.password.length == 0) {
            passwordEmpty = true;
        }

        if (this.state.confirmPassword.length == 0) {
            confirmPasswordEmpty = true;
        }

        if (passwordMatch == true && emailValid == true) {
            if (
                lastNameEmpty == true ||
                firstNameEmpty == true ||
                emailEmpty == true ||
                addressEmpty == true ||
                passwordEmpty == true ||
                confirmPasswordEmpty == true ||
                contactNumberEmpty == true
            ) {
                alert('Please fill all the fields');
                this.setState({ buttonLoading: false });
            } else {
                if (underAge != true) {
                    axios
                        .post('https://pharmakeia.herokuapp.com/customer', {
                            firstName: this.state.firstName,
                            lastName: this.state.lastName,
                            email: this.state.email,
                            contactNo: this.state.contactNumber,
                            address: this.state.address,
                            password: this.state.password,
                            birthDate: `${this.state.year}-${this.state.month}-${this.state.day}`
                        })
                        .then(res => {
                            console.log(res);
                            this.setState({
                                firstName: '',
                                lastName: '',
                                email: '',
                                contactNumber: '',
                                address: '',
                                password: '',
                                confirmPassword: '',
                                buttonLoading: false,
                                buttonDisable: false
                            });
                            alert('Account created');
                            this.props.navigation.navigate('Login');
                        })
                        .catch(error => {
                            console.log(error.response.data);
                            if (
                                error.response.data == 'Account already exist.'
                            ) {
                                alert('Account already exist');
                            }
                            this.setState({
                                firstName: '',
                                lastName: '',
                                email: '',
                                contactNumber: '',
                                address: '',
                                password: '',
                                confirmPassword: '',
                                buttonLoading: false,
                                buttonDisable: false
                            });
                        });
                }
            }
        }
    };

    validate = (text, type) => {
        switch (type) {
            case 'firstName':
                let regName = /^[a-zA-Z]+ [a-zA-Z]+$/;
                if (!regName.test(text)) {
                    alert('Invalid name given');
                } else {
                    alert('Valid name given');
                }
                break;

            default:
                break;
        }
    };

    render() {
        return (
            <KeyboardAvoidingView
                behavior="height"
                style={{
                    width: '100%',
                    alignItems: 'center',
                    justifyContent: 'center',
                    paddingVertical: 50
                }}
            >
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ width: '100%' }}
                >
                    <View
                        style={{
                            width: '100%',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                    >
                        <Text style={{ color: 'steelblue', fontSize: 20 }}>
                            Create an account
                        </Text>
                    </View>
                    <View
                        style={{
                            width: '100%',
                            alignItems: 'center',
                            justifyContent: 'center',
                            marginTop: 50
                        }}
                    >
                        <TextInput
                            style={{
                                paddingVertical: 5,
                                paddingHorizontal: 10,
                                fontSize: 15,
                                borderBottomWidth: 1,
                                borderColor: 'steelblue',
                                width: '80%',
                                color: 'steelblue',
                                margin: 0,
                                padding: 0
                            }}
                            underlineColorAndroid="rgba(0,0,0,0)"
                            placeholder="First Name"
                            placeholderTextColor="gray"
                            value={this.state.firstName}
                            onChangeText={firstName => {
                                this.validate(firstName, 'firstName');
                                // this.setState({ firstName })
                            }}
                        />
                        <TextInput
                            style={{
                                paddingVertical: 5,
                                paddingHorizontal: 10,
                                fontSize: 15,
                                borderBottomWidth: 1,
                                borderColor: 'steelblue',
                                width: '80%',
                                color: 'steelblue',
                                margin: 0,
                                padding: 0,
                                marginTop: 20
                            }}
                            underlineColorAndroid="rgba(0,0,0,0)"
                            placeholder="Last Name"
                            placeholderTextColor="gray"
                            value={this.state.lastName}
                            onChangeText={lastName =>
                                this.setState({ lastName })
                            }
                        />
                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                width: '80%'
                            }}
                        >
                            <TextInput
                                style={{
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    paddingVertical: 5,
                                    paddingHorizontal: 10,
                                    fontSize: 15,
                                    borderBottomWidth: 1,
                                    borderColor: 'steelblue',
                                    width: '30%',
                                    color: 'steelblue',
                                    marginTop: 20
                                }}
                                underlineColorAndroid="rgba(0,0,0,0)"
                                placeholder="Month"
                                placeholderTextColor="gray"
                                keyboardType="number-pad"
                                value={this.state.month}
                                onChangeText={month => this.setState({ month })}
                                maxLength={2}
                            />
                            <TextInput
                                style={{
                                    paddingVertical: 5,
                                    paddingHorizontal: 10,
                                    fontSize: 15,
                                    borderBottomWidth: 1,
                                    borderColor: 'steelblue',
                                    width: '20%',
                                    color: 'steelblue',
                                    margin: 0,
                                    padding: 0,
                                    marginTop: 20
                                }}
                                underlineColorAndroid="rgba(0,0,0,0)"
                                placeholder="Day"
                                placeholderTextColor="gray"
                                keyboardType="number-pad"
                                value={this.state.day}
                                onChangeText={day => this.setState({ day })}
                                maxLength={2}
                            />
                            <TextInput
                                style={{
                                    paddingVertical: 5,
                                    paddingHorizontal: 10,
                                    fontSize: 15,
                                    borderBottomWidth: 1,
                                    borderColor: 'steelblue',
                                    width: '30%',
                                    color: 'steelblue',
                                    margin: 0,
                                    padding: 0,
                                    marginTop: 20
                                }}
                                underlineColorAndroid="rgba(0,0,0,0)"
                                placeholder="Year"
                                placeholderTextColor="gray"
                                keyboardType="number-pad"
                                value={this.state.year}
                                onChangeText={year => this.setState({ year })}
                                maxLength={4}
                            />
                        </View>
                        <TextInput
                            style={{
                                paddingVertical: 5,
                                paddingHorizontal: 10,
                                fontSize: 15,
                                borderBottomWidth: 1,
                                borderColor: 'steelblue',
                                width: '80%',
                                color: 'steelblue',
                                margin: 0,
                                padding: 0,
                                marginTop: 20
                            }}
                            underlineColorAndroid="rgba(0,0,0,0)"
                            placeholder="Email"
                            placeholderTextColor="gray"
                            value={this.state.email}
                            onChangeText={email => this.setState({ email })}
                        />
                        <TextInput
                            style={{
                                paddingVertical: 5,
                                paddingHorizontal: 10,
                                fontSize: 15,
                                borderBottomWidth: 1,
                                borderColor: 'steelblue',
                                width: '80%',
                                color: 'steelblue',
                                margin: 0,
                                padding: 0,
                                marginTop: 20
                            }}
                            underlineColorAndroid="rgba(0,0,0,0)"
                            placeholder="Contact Number"
                            placeholderTextColor="gray"
                            keyboardType="number-pad"
                            value={this.state.contactNumber}
                            onChangeText={contactNumber =>
                                this.setState({ contactNumber })
                            }
                        />
                        <TextInput
                            style={{
                                paddingVertical: 5,
                                paddingHorizontal: 10,
                                fontSize: 15,
                                borderBottomWidth: 1,
                                borderColor: 'steelblue',
                                width: '80%',
                                color: 'steelblue',
                                margin: 0,
                                padding: 0,
                                marginTop: 20
                            }}
                            underlineColorAndroid="rgba(0,0,0,0)"
                            placeholder="Address"
                            placeholderTextColor="gray"
                            value={this.state.address}
                            onChangeText={address => this.setState({ address })}
                        />
                        <TextInput
                            secureTextEntry
                            style={{
                                paddingVertical: 5,
                                paddingHorizontal: 10,
                                fontSize: 15,
                                borderBottomWidth: 1,
                                borderColor: 'steelblue',
                                width: '80%',
                                color: 'steelblue',
                                margin: 0,
                                padding: 0,
                                marginTop: 20
                            }}
                            underlineColorAndroid="rgba(0,0,0,0)"
                            placeholder="Password"
                            placeholderTextColor="gray"
                            value={this.state.password}
                            onChangeText={password =>
                                this.setState({ password })
                            }
                        />
                        <TextInput
                            secureTextEntry
                            underlineColorAndroid="rgba(0,0,0,0)"
                            style={{
                                paddingVertical: 5,
                                paddingHorizontal: 10,
                                fontSize: 15,
                                borderBottomWidth: 1,
                                borderColor: 'steelblue',
                                width: '80%',
                                color: 'steelblue',
                                margin: 0,
                                padding: 0,
                                marginTop: 20
                            }}
                            underlineColorAndroid="rgba(0,0,0,0)"
                            placeholder="Confirm Password"
                            placeholderTextColor="gray"
                            value={this.state.confirmPassword}
                            onChangeText={confirmPassword =>
                                this.setState({ confirmPassword })
                            }
                        />
                        <TouchableOpacity
                            style={{
                                backgroundColor: '#59bfef',
                                height: 40,
                                borderRadius: 12,
                                width: '80%',
                                justifyContent: 'center',
                                alignItems: 'center',
                                marginTop: 30
                            }}
                            disabled={this.state.buttonDisable}
                            onPress={this.createAccount}
                        >
                            {this.buttonLoading()}
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{ marginTop: 10 }}
                            onPress={() =>
                                this.props.navigation.navigate('MainLogin')
                            }
                        >
                            <Text style={{ color: '#27c', fontSize: 16 }}>
                                Have an account? Sign in{' '}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        );
    }
}

export default Register;
