import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    Dimensions,
    ScrollView,
    Alert,
    TextInput
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { SearchBar } from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';
import { Icon } from 'react-native-elements';
const image = [
    'https://images.unsplash.com/photo-1471864190281-a93a3070b6de?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
    'https://stpsupport.nice.org.uk/medicines-optimisation/assets/Ga2ol3jlje/a-number-of-medicines-of-all-shapes-and-size-laid-out-on-a-table-2560x1707.jpeg',
    'https://assets.nst.com.my/images/articles/11_MedManagement_Pills_IMAGE.jpg_1517107116.jpg',
    'https://www.livelaw.in/cms/wp-content/uploads/2017/12/Online-Sale-of-Drugs.jpg'
];
class Home extends Component {
    scrollRef = React.createRef();
    state = { search: '', selectedIndex: 0, search: '' };

    componentDidMount() {
        setInterval(() => {
            this.setState(
                prev => ({
                    selectedIndex:
                        prev.selectedIndex === image.length - 1
                            ? 0
                            : prev.selectedIndex + 1
                }),
                () => {
                    this.scrollRef.current.scrollTo({
                        animated: true,
                        y: 0,
                        x: 350 * this.state.selectedIndex
                    });
                }
            );
        }, 5000);
    }

    updateSearch = search => {
        this.setState({ search });
    };

    renderCarousel = () => {
        return image.map(image => {
            <Image source={{ uri: image }} style={{ width: '100%' }} />;
        });
    };

    openCamera = () => {
        const options = {};
        ImagePicker.launchCamera(options, response => {
            console.log(response);
        });
    };

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'whitesmoke' }}>
                <View
                    style={{
                        flex: 0.1,
                        backgroundColor: '#1fa5f2',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        paddingVertical: 5
                    }}
                >
                    <View
                        style={{
                            flexDirection: 'row',
                            borderRadius: 10,
                            justifyContent: 'center',
                            alignItems: 'center',
                            paddingHorizontal: 5
                        }}
                    >
                        <TextInput
                            placeholder="Search Product Here"
                            style={{
                                width: '100%',
                                borderRadius: 10,
                                height: '80%',
                                backgroundColor: 'white',
                                paddingLeft: 15,
                                marginRight: 5,
                                flex: 0.9
                            }}
                            value={this.state.search}
                            onChangeText={text =>
                                this.setState({ search: text })
                            }
                        />
                        <TouchableOpacity
                            onPress={() =>
                                this.props.navigation.navigate('HomeProducts', {
                                    search: this.state.search
                                })
                            }
                            style={{ flex: 0.1 }}
                        >
                            <Icon
                                name="search"
                                type="font-awesome"
                                size={20}
                                color="white"
                            />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flex: 0.4, padding: 10, alignItems: 'center' }}>
                    <ScrollView
                        horizontal
                        pagingEnabled
                        showsHorizontalScrollIndicator={false}
                        style={{ width: 350, height: 500 }}
                        ref={this.scrollRef}
                    >
                        {image.map(image => (
                            <Image
                                key={image}
                                source={{ uri: image }}
                                style={{
                                    width: 350,
                                    height: '100%',
                                    backgroundColor: 'blue',
                                    borderRadius: 20
                                }}
                            />
                        ))}
                    </ScrollView>
                </View>
                <View
                    style={{
                        flex: 0.1,
                        alignItems: 'center',
                        paddingHorizontal: 30,
                        paddingVertical: 10
                    }}
                >
                    <TouchableOpacity
                        onPress={() => this.openCamera()}
                        style={{
                            backgroundColor: 'white',
                            width: '100%',
                            height: 50,
                            alignItems: 'center',
                            justifyContent: 'center',
                            flexDirection: 'row'
                        }}
                    >
                        <Icon name="camera" type="font-awesome" />
                        <Text
                            style={{
                                fontSize: 15,
                                fontFamily: 'Roboto-Regular',
                                color: 'black',
                                marginLeft: 10
                            }}
                        >
                            Upload Prescription
                        </Text>
                    </TouchableOpacity>
                </View>
                <View
                    style={{
                        flex: 0.4,
                        padding: 20,
                        justifyContent: 'space-around'
                    }}
                >
                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center'
                        }}
                    >
                        <Text style={{ fontSize: 20, color: 'gray' }}>
                            Products
                        </Text>
                        <TouchableOpacity
                            onPress={() =>
                                this.props.navigation.navigate('Product')
                            }
                        >
                            <Text style={{ fontSize: 16, color: 'black' }}>
                                See All
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ height: 150, flexDirection: 'row' }}>
                        <View
                            style={{
                                flex: 1,
                                padding: 5,
                                backgroundColor: 'white',
                                marginRight: 10
                            }}
                        >
                            <View style={{ flex: 0.5 }}>
                                <Image
                                    source={{
                                        uri:
                                            'https://mpng.pngfly.com/20171218/288/pills-png-5a37beae8e65a5.6225290815136027345833.jpg'
                                    }}
                                    style={{ height: '100%', width: '100%' }}
                                />
                            </View>
                            <View style={{ flex: 0.5 }}>
                                <Text
                                    style={{
                                        fontSize: 12,
                                        alignSelf: 'center'
                                    }}
                                >
                                    Product 1
                                </Text>
                                <Text style={{ fontSize: 10 }}>
                                    Lorem Ipsum Lorem Ipsu Dolom Ficksu
                                </Text>
                                <Text style={{ fontSize: 10 }}>100.00</Text>
                            </View>
                        </View>
                        <View
                            style={{
                                flex: 1,
                                padding: 5,
                                backgroundColor: 'white'
                            }}
                        >
                            <View style={{ flex: 0.5 }}>
                                <Image
                                    source={{
                                        uri:
                                            'https://mpng.pngfly.com/20171218/288/pills-png-5a37beae8e65a5.6225290815136027345833.jpg'
                                    }}
                                    style={{ height: '100%', width: '100%' }}
                                />
                            </View>
                            <View style={{ flex: 0.5 }}>
                                <Text
                                    style={{
                                        fontSize: 12,
                                        alignSelf: 'center'
                                    }}
                                >
                                    Product 1
                                </Text>
                                <Text style={{ fontSize: 10 }}>
                                    Lorem Ipsum Lorem Ipsu Dolom Ficksu
                                </Text>
                                <Text style={{ fontSize: 10 }}>100.00</Text>
                            </View>
                        </View>
                        <View
                            style={{
                                flex: 1,
                                padding: 5,
                                backgroundColor: 'white',
                                marginLeft: 10
                            }}
                        >
                            <View style={{ flex: 0.5 }}>
                                <Image
                                    source={{
                                        uri:
                                            'https://mpng.pngfly.com/20171218/288/pills-png-5a37beae8e65a5.6225290815136027345833.jpg'
                                    }}
                                    style={{ height: '100%', width: '100%' }}
                                />
                            </View>
                            <View style={{ flex: 0.5 }}>
                                <Text
                                    style={{
                                        fontSize: 12,
                                        alignSelf: 'center'
                                    }}
                                >
                                    Product 1
                                </Text>
                                <Text style={{ fontSize: 10 }}>
                                    Lorem Ipsum Lorem Ipsu Dolom Ficksu
                                </Text>
                                <Text style={{ fontSize: 10 }}>100.00</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = {
    circleDiv: {
        position: 'absolute',
        bottom: 15,
        height: 10,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    whiteCircle: {
        width: 6,
        height: 6,
        borderRadius: 3,
        margin: 5,
        backgroundColor: 'black'
    }
};
export default Home;
