import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import { Icon, Badge } from 'react-native-elements';
import axios from 'axios';
import { LoginManager } from 'react-native-fbsdk';
import AsyncStorage from '@react-native-community/async-storage';

class Profile extends Component {
    state = {
        customerData: [],
        forApprovalLength: 0,
        forApproval: [],
        forReceiving: [],
        forReceivingLength: 0,
        apiLoading: true,
        forFeedback: [],
        forFeedbackLength: 0
    };
    componentDidMount = async () => {
        let customerData = await AsyncStorage.getItem('customerData');
        customerData = JSON.parse(customerData);
        console.log(customerData);
        axios
            .get(`http://pharmakeia.herokuapp.com/customer/${customerData._id}`)
            .then(res => {
                this.setState({ customerData: res.data });
            })
            .catch(error => {
                console.log(error);
            });
    };

    getCustomerData = async () => {
        let customerData = await AsyncStorage.getItem('customerData');
        customerData = JSON.parse(customerData);

        axios
            .get(`http://pharmakeia.herokuapp.com/customer/${customerData._id}`)
            .then(res => {
                this.setState({ customerData: res.data });
                return axios.get(
                    `http://pharmakeia.herokuapp.com/order/customer/${customerData._id}/status/for approval`
                );
            })
            .then(res => {
                const forApproval = res.data;
                this.setState({ forApproval: forApproval });
                this.setState({ forApprovalLength: forApproval.length });

                return axios.get(
                    `http://pharmakeia.herokuapp.com/order/customer/${customerData._id}/status/for delivery`
                );
            })
            .then(res => {
                const forReceiving = res.data;
                this.setState({ forReceiving: forReceiving });
                this.setState({ forReceivingLength: forReceiving.length });

                return axios.get(
                    `http://pharmakeia.herokuapp.com/order/toreview/customer/${customerData._id}`
                );
            })
            .then(res => {
                const forFeedback = res.data;
                console.log('Rap', JSON.stringify(forFeedback, '', 4));
                this.setState({ forFeedback: forFeedback });
                this.setState({ forFeedbackLength: forFeedback.length });
                this.setState({ apiLoading: false });
            })
            .catch(error => {
                console.log(error);
            });
        console.log('state', this.state.customerData);
    };

    logoutFacebook = () => {
        LoginManager.logOut(function(res) {
            console.log(res);
        });
        this.props.navigation.navigate('Login');
    };

    render() {
        return (
            <View style={{ flex: 1 }}>
                <NavigationEvents onWillFocus={() => this.getCustomerData()} />
                <View
                    style={{
                        flex: 0.3,
                        backgroundColor: '#1fa5f2',
                        flexDirection: 'column-reverse'
                    }}
                >
                    <Text
                        style={{
                            color: 'white',
                            fontSize: 18,
                            paddingBottom: 20,
                            paddingLeft: 20
                        }}
                    >
                        {`${this.state.customerData.firstName} ${this.state.customerData.lastName}`}
                    </Text>
                </View>
                <View style={{ flex: 0.7 }}>
                    <Text
                        style={{
                            paddingLeft: 20,
                            paddingVertical: 10,
                            fontSize: 18,
                            fontWeight: 'bold',
                            borderBottomWidth: 1,
                            borderColor: 'whitesmoke'
                        }}
                    >
                        My Purchases
                    </Text>
                    <View
                        style={{
                            height: 100,
                            flexDirection: 'row',
                            borderColor: 'whitesmoke',
                            borderBottomWidth: 1
                        }}
                    >
                        <View
                            style={{
                                flex: 1,
                                flexDirection: 'column-reverse',
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}
                        >
                            <TouchableOpacity
                                onPress={() =>
                                    this.props.navigation.navigate(
                                        'ForApproval',
                                        this.state.forApproval
                                    )
                                }
                            >
                                <View>
                                    <Icon
                                        name="thumbs-up"
                                        type="font-awesome"
                                        size={40}
                                    />
                                    <Badge
                                        badgeStyle={{
                                            backgroundColor: '#1fa5f2'
                                        }}
                                        value={this.state.forApprovalLength}
                                        status="success"
                                        containerStyle={{
                                            position: 'absolute',
                                            top: 0,
                                            right: 0
                                        }}
                                    />
                                </View>
                                <Text>For Approval</Text>
                            </TouchableOpacity>
                        </View>
                        <View
                            style={{
                                flex: 1,
                                flexDirection: 'column-reverse',
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}
                        >
                            <TouchableOpacity
                                onPress={() =>
                                    this.props.navigation.navigate(
                                        'ForReceiving',
                                        this.state.forReceiving
                                    )
                                }
                            >
                                <View>
                                    <Icon
                                        name="truck"
                                        type="font-awesome"
                                        size={40}
                                    />
                                    <Badge
                                        badgeStyle={{
                                            backgroundColor: '#1fa5f2'
                                        }}
                                        value={this.state.forReceivingLength}
                                        status="success"
                                        containerStyle={{
                                            position: 'absolute',
                                            top: 0,
                                            right: -20
                                        }}
                                    />
                                </View>
                                <Text>For Receiving</Text>
                            </TouchableOpacity>
                        </View>
                        <View
                            style={{
                                flex: 1,
                                flexDirection: 'column-reverse',
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}
                        >
                            <TouchableOpacity
                                onPress={() =>
                                    this.props.navigation.navigate(
                                        'ForFeedback',
                                        this.state.forFeedback
                                    )
                                }
                            >
                                <View>
                                    <Icon
                                        name="star"
                                        type="font-awesome"
                                        size={40}
                                    />
                                    <Badge
                                        badgeStyle={{
                                            backgroundColor: '#1fa5f2'
                                        }}
                                        value={this.state.forFeedbackLength}
                                        status="success"
                                        containerStyle={{
                                            position: 'absolute',
                                            top: 0,
                                            right: -20
                                        }}
                                    />
                                </View>
                                <Text>Feedback</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <TouchableOpacity
                        onPress={() =>
                            this.props.navigation.navigate('AccountSettings')
                        }
                        style={{
                            paddingLeft: 20,
                            paddingVertical: 10,
                            borderBottomWidth: 1,
                            borderColor: 'whitesmoke'
                        }}
                    >
                        <Text style={{ fontSize: 17 }}>Account Settings</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() =>
                            this.props.navigation.navigate('ChangePassword')
                        }
                        style={{
                            paddingLeft: 20,
                            paddingVertical: 10,
                            borderBottomWidth: 1,
                            borderColor: 'whitesmoke'
                        }}
                    >
                        <Text style={{ fontSize: 17 }}>Change Password</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            AsyncStorage.clear();
                            this.props.navigation.navigate('Login');
                        }}
                        style={{
                            paddingLeft: 20,
                            paddingVertical: 10,
                            borderBottomWidth: 1,
                            borderColor: 'whitesmoke'
                        }}
                    >
                        <Text style={{ fontSize: 17 }}>Logout</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

export default Profile;
