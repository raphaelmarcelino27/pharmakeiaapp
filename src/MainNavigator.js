import React from 'react';
import Login from './components/Login';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Register from './components/Register';
import MainLogin from './components/MainLogin';
import InsideNavigator from './components/InsideNavigator';
import ProductDetails from './components/ProductDetails';
import Shipping from './components/checkout/Shipping';
import Payment from './components/checkout/Payment';
import Confirm from './components/checkout/Confirm';

const AppNavigator = createStackNavigator(
    {
        Login: Login,
        Register: Register,
        MainLogin: MainLogin,
        InsideNavigator: InsideNavigator
    },
    {
        initialRouteName: 'InsideNavigator',
        headerMode: 'none'
    }
);

const MainNavigator = createAppContainer(AppNavigator);

export default MainNavigator;
